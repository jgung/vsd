package edu.colorado.clear.nlp.application.pred;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.ml.classifier.BaseClassifier;
import edu.colorado.clear.ml.classifier.ClassifierWrapper;
import edu.colorado.clear.ml.classifier.ClassifierWrapperModel;
import edu.colorado.clear.ml.classifier.ParameterizedModel;
import edu.colorado.clear.ml.classifier.PassiveAggressive;
import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.nlp.application.SentenceProcessor;
import edu.colorado.clear.nlp.experiment.Evaluation;
import edu.colorado.clear.nlp.feature.AggregateFeatureExtractor;
import edu.colorado.clear.nlp.feature.Context;
import edu.colorado.clear.nlp.feature.FeatureExtractor;
import edu.colorado.clear.nlp.feature.MapFeatureExtractor;
import edu.colorado.clear.nlp.parser.ConllDepReader;
import edu.colorado.clear.nlp.type.Feats;
import edu.colorado.clear.nlp.type.Token;
import edu.colorado.clear.nlp.type.VnSentence;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class PredicateClassifier extends ClassifierWrapper<ParameterizedModel, ClassifierWrapperModel<ParameterizedModel>>
        implements SentenceProcessor {

    public static final String PREDICATE = "Yes";
    public static final String NOT_PREDICATE = "No";

    private List<String> dependencyContexts = Arrays.asList("nsubj", "dobj", "nsubjpass", "iobj", "nmodC");
    private List<Integer> contextSizes = Arrays.asList(-2, -1, 0, 1, 2);

    private final List<String> rolesets = Arrays.asList("be.03", "do.01, have.01", "become.01");

    private AggregateFeatureExtractor windowFeatureExtractor;
    private AggregateFeatureExtractor dependencyFeatureExtractor;
    private AggregateFeatureExtractor depPathFeatureExtractor;

    public PredicateClassifier() {
        initializeExtractors();
    }

    public void initializeExtractors() {

        FeatureExtractor lemmaFeatureExtractor = new MapFeatureExtractor(Feats.Lemma.getKey());
        FeatureExtractor formFeatureExtractor = new MapFeatureExtractor(Feats.Form.getKey());
        FeatureExtractor posFeatureExtractor = new MapFeatureExtractor(Feats.Pos.getKey());
        FeatureExtractor depFeatureExtractor = new MapFeatureExtractor(Feats.Dep.getKey());

        windowFeatureExtractor = new AggregateFeatureExtractor(lemmaFeatureExtractor, formFeatureExtractor,
                posFeatureExtractor, depFeatureExtractor);

        dependencyFeatureExtractor = new AggregateFeatureExtractor(lemmaFeatureExtractor, formFeatureExtractor,
                posFeatureExtractor);

        depPathFeatureExtractor = new AggregateFeatureExtractor(posFeatureExtractor,
                depFeatureExtractor);
    }

    @Override
    public List<LabeledVector> loadInstances(String path, boolean training) {
        List<VnSentence> sentences = new ConllDepReader().readSentences(new File(path));
        List<LabeledVector> vectors = new ArrayList<>();

        for (VnSentence sentence : sentences) {
            vectors.addAll(sentence.getTokens().stream()
                    .map(token -> extractInstance(sentence, token, training)).collect(Collectors.toList()));
        }

        return vectors;
    }

    private LabeledVector extractInstance(VnSentence sentence, Token focus, boolean training) {
        List<Feature> features = new ArrayList<>();

        // collocational features
        for (Context context : Context.getWindow(sentence.getTokens(), focus.getIndex(), contextSizes)) {
            features.addAll(windowFeatureExtractor.extract(context));
        }

        for (Context context : Context.getDependencyContexts(focus)) {
            features.addAll(dependencyFeatureExtractor.extract(context));
        }

        // root path features
        features.addAll(depPathFeatureExtractor.extract(Context.getRootPathContext(focus)));
        for (Context context : Context.getChildModifierFeatures(focus, dependencyContexts)) {
            features.addAll(depPathFeatureExtractor.extract(context));
        }
        for (Context context : Context.getChildSkipModifierFeatures(focus, dependencyContexts)) {
            features.addAll(depPathFeatureExtractor.extract(context));
        }

        LabeledVector instance = classifier.createInstance(features,
                focus.getSense() != null ? (isPredicate(focus.getSense()) ? PREDICATE : NOT_PREDICATE) : null, training);
        instance.setId(sentence.getTokens().stream().map(Token::getForm).collect(Collectors.joining(" ")) + " - "
                + focus.getIndex() + "-" + focus.getForm());
        return instance;
    }

    private boolean isPredicate(String sense) {
        return !(sense.equals("-") || rolesets.contains(sense));
    }

    @Override
    public BaseClassifier<ParameterizedModel> loadClassifier() {
        return new PassiveAggressive();
    }

    @Override
    public void saveModel(OutputStream outputStream) {
        model.setWrappedModel(classifier.getModel());
        super.saveModel(outputStream);
    }


    @Override
    public ClassifierWrapperModel<ParameterizedModel> initializeModel() {
        return new ClassifierWrapperModel<>();
    }

    @Override
    public void process(VnSentence input) {
        input.getTokens()
                .forEach(token -> {
                    LabeledVector instance = extractInstance(input, token, false);
                    token.setSense(apply(instance));
                });
    }

    public void process(VnSentence input, Token token) {
        LabeledVector instance = extractInstance(input, token, false);
        token.setSense(apply(instance));
    }

    public static void main(String... args) throws IOException {
        PredicateClassifier wsd = new PredicateClassifier();
        wsd.initialize();

        List<LabeledVector> trainData = wsd.loadInstances("data/datasets/conll2012/dep/train-preds.dep", true);

        wsd.train(trainData, new ArrayList<>());
        wsd.saveModel(new FileOutputStream("data/models/pred.model"));

        wsd = new PredicateClassifier();
        wsd.loadModel(new FileInputStream("data/models/pred.model"));

        List<LabeledVector> testInstances = wsd.loadInstances("data/datasets/conll2012/dep/test-preds.dep", false);
        Evaluation evaluation = new Evaluation();
        for (LabeledVector vector : testInstances) {
            String result = wsd.apply(vector);
            evaluation.add(result, vector.getLabel());
            if (!result.equals(vector.getLabel())) {
                System.out.println(result);
                System.out.println(vector.getId());
            }
        }
        System.out.println(evaluation.toString());
        System.out.println(evaluation.getMatrix().toString());
    }

}
