package edu.colorado.clear.nlp.application;

import java.util.List;

/**
 * @author jamesgung
 */
public interface TokenLabeler {

    List<String> label(List<String> tokens);

}
