package edu.colorado.clear.nlp.application;

import edu.colorado.clear.nlp.type.VnSentence;

/**
 * @author jamesgung
 */
public interface SentenceProcessor {

    void process(VnSentence input);

}
