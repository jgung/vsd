package edu.colorado.clear.nlp.application.vsd;

import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.ml.classifier.BaseClassifier;
import edu.colorado.clear.ml.classifier.ClassifierWrapper;
import edu.colorado.clear.ml.classifier.ClassifierWrapperModel;
import edu.colorado.clear.ml.classifier.ParameterizedModel;
import edu.colorado.clear.ml.classifier.PassiveAggressive;
import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.nlp.application.SentenceProcessor;
import edu.colorado.clear.nlp.application.SimpleFeatureBank;
import edu.colorado.clear.nlp.feature.AggregateFeatureExtractor;
import edu.colorado.clear.nlp.feature.BrownClusterFeatureExtractor;
import edu.colorado.clear.nlp.feature.ClusterFeatureExtractor;
import edu.colorado.clear.nlp.feature.Context;
import edu.colorado.clear.nlp.feature.FeatureExtractor;
import edu.colorado.clear.nlp.feature.MapFeatureExtractor;
import edu.colorado.clear.nlp.parser.ConllDepVnReader;
import edu.colorado.clear.nlp.type.Feats;
import edu.colorado.clear.nlp.type.Token;
import edu.colorado.clear.nlp.type.VnSentence;
import edu.colorado.clear.util.PosUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class VerbNetSingle extends ClassifierWrapper<ParameterizedModel, VerbNetSingle.SimpleWsdModel>
        implements SentenceProcessor {

    @Parameter(names = "-data", description = "path to feature bank model")
    private String dataPath = "edu/colorado/clear/nlp/models/feature-extractors.model";
    private List<String> dependencyContexts = Arrays.asList("nsubj", "dobj", "nsubjpass", "iobj", "nmodC");
    private List<Integer> contextSizes = Arrays.asList(-2, -1, 0, 1, 2);

    private AggregateFeatureExtractor windowFeatureExtractor;
    private AggregateFeatureExtractor dependencyFeatureExtractor;
    private AggregateFeatureExtractor filteredDependencyFeatureExtractor;
    private AggregateFeatureExtractor depPathFeatureExtractor;

    private SetMultimap<String, String> validLabels; // map used to constrain predictions to valid labels

    @Override
    public SimpleWsdModel initializeModel() {
        return new SimpleWsdModel();
    }

    private void initalizeParameters(String... args) {
        JCommander jCommander = new JCommander(this);
        jCommander.parse(args);
    }

    @Override
    public BaseClassifier<ParameterizedModel> loadClassifier() {
        return new PassiveAggressive();
    }

    @Override
    public void initialize() {
        super.initialize();
        validLabels = HashMultimap.create();
    }

    @Override
    public void initialize(SimpleWsdModel model) {
        super.initialize(model);
        validLabels = model.getValidLabels();
    }

    public void initializeArgs(String... args) {
        initalizeParameters(args);

        SimpleFeatureBank extractors = SimpleFeatureBank.loadLearningResources(dataPath);
        BrownClusterFeatureExtractor brownExtractors = extractors.getBrownExtractor();
        List<ClusterFeatureExtractor> clusterExtractors = extractors.getClusterExtractors();

        FeatureExtractor lemmaFeatureExtractor = new MapFeatureExtractor(Feats.Lemma.getKey());
        FeatureExtractor formFeatureExtractor = new MapFeatureExtractor(Feats.Form.getKey());
        FeatureExtractor posFeatureExtractor = new MapFeatureExtractor(Feats.Pos.getKey());
        FeatureExtractor depFeatureExtractor = new MapFeatureExtractor(Feats.Dep.getKey());

        windowFeatureExtractor = new AggregateFeatureExtractor(lemmaFeatureExtractor, formFeatureExtractor,
                posFeatureExtractor, depFeatureExtractor);

        dependencyFeatureExtractor = new AggregateFeatureExtractor(lemmaFeatureExtractor, formFeatureExtractor,
                posFeatureExtractor);

        filteredDependencyFeatureExtractor = new AggregateFeatureExtractor(lemmaFeatureExtractor, formFeatureExtractor,
                posFeatureExtractor);

        depPathFeatureExtractor = new AggregateFeatureExtractor(posFeatureExtractor,
                depFeatureExtractor);

        filteredDependencyFeatureExtractor.addAll(clusterExtractors);
        filteredDependencyFeatureExtractor.add(brownExtractors);
    }

    @Override
    public List<LabeledVector> loadInstances(String path, boolean training) {
        List<LabeledVector> instances = new ArrayList<>();
        Stopwatch stopwatch = Stopwatch.createStarted();
        List<VnSentence> sentences = new ConllDepVnReader().readSentences(new File(path));
        log.info("Loaded {} pre-parsed instances from {} in {}", sentences.size(), path, stopwatch.stop());
        stopwatch = Stopwatch.createStarted();
        for (VnSentence sentence : sentences) {
            List<LabeledVector> add = sentence.getInstances().stream()
                    .map(instance -> extractInstance(sentence, sentence.getTokens().get(instance.getTokenIndex()), training))
                    .collect(Collectors.toList());
            instances.addAll(add);
        }
        log.info("Loaded features for {} instances from {} in {}", instances.size(), path, stopwatch.stop());
        return instances;
    }

    private LabeledVector extractInstance(VnSentence sentence, Token focus, boolean training) {
        List<Feature> features = new ArrayList<>();

        // collocational features
        for (Context context : Context.getWindow(sentence.getTokens(), focus.getIndex(), contextSizes)) {
            features.addAll(windowFeatureExtractor.extract(context));
        }

        // dependency context features
        for (Context context : Context.getDependencyContexts(focus, dependencyContexts)) {
            features.addAll(filteredDependencyFeatureExtractor.extract(context));
        }
        for (Context context : Context.getDependencyContexts(focus)) {
            features.addAll(dependencyFeatureExtractor.extract(context));
        }

        // root path features
        features.addAll(depPathFeatureExtractor.extract(Context.getRootPathContext(focus)));
        for (Context context : Context.getChildModifierFeatures(focus, dependencyContexts)) {
            features.addAll(depPathFeatureExtractor.extract(context));
        }
        for (Context context : Context.getChildSkipModifierFeatures(focus, dependencyContexts)) {
            features.addAll(depPathFeatureExtractor.extract(context));
        }
        // add to label map (used to constrain predictions to valid labels)
        if (training) {
            validLabels.put(focus.getLemma(), focus.getSense());
        }

        LabeledVector instance = classifier.createInstance(features, focus.getSense(), training);
        instance.setId(focus.getLemma());
        return instance;
    }

    @Override
    public String apply(LabeledVector instance) {
        float max = -Float.MAX_VALUE;
        String maxLabel = "";
        for (Map.Entry<String, Float> entry : score(instance).entrySet()) {
            float score = entry.getValue();
            if (score > max) {
                max = score;
                maxLabel = entry.getKey();
            }
        }
        return maxLabel;
    }

    @Override
    public Map<String, Float> score(LabeledVector instance) {
        Map<String, Float> scores = classifier.score(instance);
        if (validLabels.containsKey(instance.getId())) {
            Map<String, Float> newMap = new HashMap<>();
            Set<String> labels = validLabels.get(instance.getId());
            for (String label : labels) {
                newMap.put(label, scores.get(label));
            }
            return newMap;
        } else {
            return new HashMap<>();
        }
    }

    @Override
    public float test(List<LabeledVector> test) {
        int correct = 0;
        for (LabeledVector vector : test) {
            String prediction = apply(vector);
            if (prediction.equals(vector.getLabel())) {
                correct++;
            }
        }
        return (float) correct / test.size();
    }

    @Override
    public void saveModel(OutputStream outputStream) {
        model.setValidLabels(validLabels);
        model.setWrappedModel(classifier.getModel());
        super.saveModel(outputStream);
    }

    @Override
    public void process(VnSentence input) {
        input.getTokens().stream()
                .filter(token -> PosUtils.isVerb(token.getPos()) && !token.getDep().equals("aux"))
                .forEach(token -> {
                    LabeledVector instance = extractInstance(input, token, false);
                    token.setSense(apply(instance));
                });
    }

    public void process(VnSentence input, Token token) {
        LabeledVector instance = extractInstance(input, token, false);
        token.setSense(apply(instance));
    }

    static class SimpleWsdModel extends ClassifierWrapperModel<ParameterizedModel> {

        @Getter
        @Setter
        private SetMultimap<String, String> validLabels; // map used to constrain predictions to valid labels

    }

}
