package edu.colorado.clear.nlp.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import edu.colorado.clear.nlp.feature.BrownClusterFeatureExtractor;
import edu.colorado.clear.nlp.feature.ClusterFeatureExtractor;
import edu.colorado.clear.nlp.feature.DDNFeatureExtractor;
import edu.colorado.clear.nlp.feature.WordNetFeatureExtractor;
import edu.colorado.clear.util.JwiWordNetInterface;
import edu.colorado.clear.util.LuceneInterface;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class FeatureExtractorBank {

    @Getter
    private List<ClusterFeatureExtractor> clusterExtractors;

    @Getter
    private DDNFeatureExtractor ddnExtractors;

    @Getter
    private WordNetFeatureExtractor wordNetExtractor;

    @Getter
    private BrownClusterFeatureExtractor brownExtractor;

    @Setter
    private String bwcPath = "bwc.txt";
    @Setter
    private String clustersPath = "clusters";
    @Setter
    private String ddnPath = "ddnIndex";
    @Setter
    private String wordNetPath = "WordNet-3.0";
    @Setter
    private String dataPath;

    public FeatureExtractorBank(String dataPath) {
        this(dataPath, true, true, true, true);
    }

    public FeatureExtractorBank(String dataPath, boolean clusters, boolean brown, boolean wn, boolean ddn) {
        this.dataPath = dataPath;
        if (clusters) {
            loadClusters();
        }
        if (brown) {
            loadBrown();
        }
        if (wn) {
            loadWordNet();
        }
        if (ddn) {
            loadDdn();
        }
    }

    private void loadWordNet() {
        wordNetExtractor = new WordNetFeatureExtractor(
                new JwiWordNetInterface(new File(dataPath, wordNetPath)));
    }

    private void loadBrown() {
        try {
            brownExtractor = new BrownClusterFeatureExtractor(bwcPath, new FileInputStream(new File(dataPath, bwcPath)));
        } catch (FileNotFoundException e) {
            log.error("Error loading brown clusters.", e);
        }
    }

    private void loadClusters() {
        clusterExtractors = new ArrayList<>();
        File clusterDir = new File(dataPath, clustersPath);
        try {
            //noinspection ConstantConditions
            for (File file : clusterDir.listFiles()) {
                clusterExtractors.add(new ClusterFeatureExtractor(file.getName(), true,
                        new FileInputStream(file)));
            }
        } catch (FileNotFoundException e) {
            log.error("Error loading clusters.", e);
        }
    }

    private void loadDdn() {
        ddnExtractors = new DDNFeatureExtractor(new LuceneInterface(new File(dataPath, ddnPath)));
    }

}
