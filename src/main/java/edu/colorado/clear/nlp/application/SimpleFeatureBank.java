package edu.colorado.clear.nlp.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.colorado.clear.nlp.feature.BrownClusterFeatureExtractor;
import edu.colorado.clear.nlp.feature.ClusterFeatureExtractor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class SimpleFeatureBank implements Serializable {

    @Getter
    private List<ClusterFeatureExtractor> clusterExtractors;

    @Getter
    private BrownClusterFeatureExtractor brownExtractor;

    @Setter
    private String bwcPath = "bwc.txt";
    @Setter
    private String clustersPath = "clusters";
    @Setter
    private String dataPath;

    public SimpleFeatureBank(String dataPath) {
        this(dataPath, true, true);
    }

    public SimpleFeatureBank(String dataPath, boolean clusters, boolean brown) {
        this.dataPath = dataPath;
        if (clusters) {
            loadClusters();
        }
        if (brown) {
            loadBrown();
        }
    }

    private void loadBrown() {
        try {
            brownExtractor = new BrownClusterFeatureExtractor(bwcPath, new FileInputStream(new File(dataPath, bwcPath)));
        } catch (FileNotFoundException e) {
            log.error("Error loading brown clusters.", e);
        }
    }

    private void loadClusters() {
        clusterExtractors = new ArrayList<>();
        File clusterDir = new File(dataPath, clustersPath);
        try {
            //noinspection ConstantConditions
            for (File file : clusterDir.listFiles()) {
                clusterExtractors.add(new ClusterFeatureExtractor(file.getName(), true,
                        new FileInputStream(file)));
            }
        } catch (FileNotFoundException e) {
            log.error("Error loading clusters.", e);
        }
    }

    public static SimpleFeatureBank loadLearningResources(String path) {
        try {
            ObjectInputStream ois = new ObjectInputStream(SimpleFeatureBank.class.getClassLoader().getResourceAsStream(path));
            SimpleFeatureBank resources = (SimpleFeatureBank) ois.readObject();
            ois.close();
            return resources;
        } catch (NullPointerException | IOException | ClassNotFoundException e) {
            log.error("Unable to load feature extractor bank.", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Convenience method for saving learning new learning resources serializations.
     *
     * @param args none
     */
    public static void main(String... args) throws Throwable {
        SimpleFeatureBank extractorBank = new SimpleFeatureBank("data/learningResources");
        FileOutputStream fos = new FileOutputStream("data/feature-extractors.model");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(extractorBank);
        oos.close();

        // verification
        FileInputStream fis = new FileInputStream("data/feature-extractors.model");
        ObjectInputStream ois = new ObjectInputStream(fis);
        extractorBank = (SimpleFeatureBank) ois.readObject();
        System.out.println(extractorBank.clustersPath);
    }

}
