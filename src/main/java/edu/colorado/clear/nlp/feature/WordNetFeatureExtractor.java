package edu.colorado.clear.nlp.feature;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.nlp.type.Feats;
import edu.colorado.clear.nlp.type.Token;
import edu.colorado.clear.util.WordNetInterface;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public class WordNetFeatureExtractor implements FeatureExtractor {

    @Getter
    @Setter
    private WordNetInterface wn;

    public WordNetFeatureExtractor(WordNetInterface wn) {
        this.wn = wn;
    }

    @Override
    public List<Feature> extract(Context context) {
        List<Feature> features = new ArrayList<>();
        for (Token token : context.getTokens()) {
            Set<String> hypernyms = wn.getHypernyms(token.getLemma(), token.getPos());
            features.addAll(hypernyms.stream()
                    .map(hypernym -> new Feature(context, Feats.WordNet.getKey(), hypernym))
                    .collect(Collectors.toList()));
        }
        return features;
    }
}
