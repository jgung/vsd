package edu.colorado.clear.nlp.feature;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.colorado.clear.ml.Feature;
import lombok.Getter;

/**
 * @author jamesgung
 */
public class AggregateFeatureExtractor implements FeatureExtractor {

    @Getter
    private List<FeatureExtractor> extractors = new ArrayList<>();

    public AggregateFeatureExtractor(FeatureExtractor... extractors) {
        this(new ArrayList<>(Arrays.asList(extractors)));
    }

    public AggregateFeatureExtractor(List<FeatureExtractor> extractors) {
        this.extractors = extractors;
    }

    public void add(FeatureExtractor extractor) {
        extractors.add(extractor);
    }

    public void addAll(List<? extends FeatureExtractor> extractors) {
        this.extractors.addAll(extractors);
    }


    @Override
    public List<Feature> extract(Context context) {
        List<Feature> features = new ArrayList<>();
        for (FeatureExtractor extractor : extractors) {
            features.addAll(extractor.extract(context));
        }
        return features;
    }

}
