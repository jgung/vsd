package edu.colorado.clear.nlp.feature;

import com.google.common.base.Stopwatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.nlp.type.Feats;
import edu.colorado.clear.nlp.type.Token;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class BrownClusterFeatureExtractor implements FeatureExtractor {

    @Getter
    @Setter
    private int[] prefixes = {4, 6, 10, 20};
    @Getter
    @Setter
    private boolean lower = false;

    private Map<String, Map<String, String>> clusters;

    public BrownClusterFeatureExtractor(String key, InputStream clusterFile) {
        clusters = new HashMap<>();
        Stopwatch stopwatch = Stopwatch.createStarted();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(clusterFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split("\\t");
                if (fields.length < 2) {
                    continue;
                }
                String word = fields[1];
                String cluster = fields[0];
                Map<String, String> featureMap = new HashMap<>();
                for (int i : prefixes) {
                    featureMap.put(key + Feats.Brown.getKey() + "[" + i + "]",
                            cluster.substring(0, (cluster.length() > i ? i : cluster.length())));
                }
                clusters.put(word, featureMap);
            }
            log.info("Loaded brown clusters {} ({})", key, stopwatch.stop());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Feature> extract(Context context) {
        List<Feature> features = new ArrayList<>();
        for (Token token : context.getTokens()) {
            Map<String, String> featureMap = clusters.get(token.getForm());
            if (featureMap == null) {
                featureMap = clusters.get(token.getLemma());
                if (featureMap == null) {
                    continue;
                }
            }
            for (String key : featureMap.keySet()) {
                features.add(new Feature(context, key, featureMap.get(key)));
            }
        }
        return features;
    }
}
