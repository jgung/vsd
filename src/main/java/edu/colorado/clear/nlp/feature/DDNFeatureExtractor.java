package edu.colorado.clear.nlp.feature;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.nlp.type.Feats;
import edu.colorado.clear.nlp.type.Token;
import edu.colorado.clear.util.LuceneInterface;
import edu.colorado.clear.util.PosUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class DDNFeatureExtractor implements FeatureExtractor {

    private static final Pattern tokenPattern = Pattern.compile("^[a-z]+$");

    @Getter
    private String field = "object";
    @Setter
    private int maxDDNs = 50; // Maximum number of DDN features
    @Setter
    private int maxSearch = 1000; // Maximum number of hits when searching Lucene index
    @Getter
    private LuceneInterface ddnIndex;

    private Cache<String, Set<String>> ddnCache;

    public DDNFeatureExtractor(LuceneInterface ddnIndex) {
        this.ddnIndex = ddnIndex;
        ddnCache = CacheBuilder.newBuilder().build();
    }

    @Override
    public List<Feature> extract(Context context) {
        List<Feature> features = new ArrayList<>();
        for (Token token : context.getTokens()) {
            features.addAll(getDDNFeatures(context, token));
        }
        return features;
    }

    private List<Feature> getDDNFeatures(Context context, Token token) {
        return this.getDynamicDependencyNeighbors(token).stream()
                .map(ddnString -> new Feature(context, Feats.DDN.getKey(), ddnString))
                .collect(Collectors.toList());
    }

    private Set<String> getDynamicDependencyNeighbors(Token token) {
        if (!PosUtils.isNoun(token.getPos()) || !tokenPattern.matcher(token.getForm().toLowerCase()).matches()) {
            return new HashSet<>();
        }
        Set<String> ddnFeature = ddnCache.getIfPresent(token.getLemma());
        if (ddnFeature != null) {
            return ddnFeature;
        }
        ddnFeature = ddnIndex.search(token.getLemma(), field, maxSearch)
                .entrySet().stream()
                .sorted((e1, e2) -> e2.getValue() - e1.getValue())
                .map(Map.Entry::getKey)
                .limit(maxDDNs)
                .collect(Collectors.toSet());
        ddnCache.put(token.getLemma(), ddnFeature);
        return ddnFeature;
    }

}
