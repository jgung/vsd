package edu.colorado.clear.nlp.feature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.nlp.type.Token;
import lombok.Getter;

/**
 * @author jamesgung
 */
public class MapFeatureExtractor implements FeatureExtractor {

    @Getter
    private String key;

    public MapFeatureExtractor(String key) {
        this.key = key;
    }

    @Override
    public List<Feature> extract(Context context) {
        List<String> values = new ArrayList<>();
        for (Token token : context.getTokens()) {
            String result = token.getFeature(key);
            if (result != null) {
                values.add(result);
            }
        }
        return Collections.singletonList(new Feature(context, key, values));
    }

}
