package edu.colorado.clear.nlp.feature;

import java.io.Serializable;
import java.util.List;

import edu.colorado.clear.ml.Feature;

/**
 * @author jamesgung
 */
public interface FeatureExtractor extends Serializable {

    List<Feature> extract(Context context);

}
