package edu.colorado.clear.nlp.feature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import edu.colorado.clear.nlp.type.Token;
import lombok.Getter;

/**
 * @author jamesgung
 */
public class Context {

    private static final String ROOT_PATH = "RP";

    @Getter
    private List<Token> tokens;
    @Getter
    private String label;

    public Context(String label, List<Token> tokens) {
        this.label = label;
        this.tokens = tokens;
    }

    public Context(String label, Token token) {
        this(label, Collections.singletonList(token));
    }

    public static List<Context> getNgramContexts(List<Token> tokens, int length) {
        return getNgramContexts(tokens, length, "", false);
    }

    public static List<Context> getNgramContexts(List<Token> tokens, int length, String id, boolean count) {
        List<Context> contexts = new ArrayList<>();
        String ngramId = id + "{" + length + "gram" + "}";
        for (int i = 0; i < tokens.size(); ++i) {
            List<Token> ngram = new ArrayList<>();
            int end = Math.min(i + length, tokens.size());
            for (int index = i; index < end; ++index) {
                ngram.add(tokens.get(index));
            }
            if (ngram.size() > 0) {
                contexts.add(new Context((count ? i + "/" : "") + ngramId, ngram));
            }
        }
        return contexts;
    }

    private static final String WINDOW = "WIN";

    public static List<Context> getWindow(List<Token> tokens, int center, List<Integer> offsets) {
        return getWindow(tokens, center, offsets, WINDOW);
    }

    public static List<Context> getWindow(List<Token> tokens, int center, List<Integer> offsets, String id) {
        List<Context> contexts = new ArrayList<>();
        for (Integer i : offsets) {
            int containerIndex = center + i;
            if (containerIndex < 0 || containerIndex >= tokens.size()) {
                continue;
            }
            contexts.add(new Context(id + "{" + i + "}", Collections.singletonList(tokens.get(containerIndex))));
        }
        return contexts;
    }

    public static List<Context> getDependencyContexts(Token token, List<String> filter) {
        return token.getChildren().stream()
                .filter(child -> filter.contains(child.getDep()))
                .map(child -> new Context(child.getDep(), child))
                .collect(Collectors.toList());
    }

    public static List<Context> getDependencyContexts(Token token) {
        return token.getChildren().stream()
                .map(child -> new Context(child.getDep(), child))
                .collect(Collectors.toList());
    }

    public static Context getRootPathContext(Token token) {
        return new Context(ROOT_PATH, getRootPath(token));
    }

    public static List<Context> getRootPathNgrams(Token token, int length) {
        return getNgramContexts(getRootPath(token), length, ROOT_PATH, true);
    }

    public static List<Context> getChildModifierFeatures(Token token, List<String> filter) {
        List<Context> context = new ArrayList<>();
        token.getChildren().stream()
                .filter(c -> filter.contains(c.getDep()))
                .forEach(c -> context.addAll(c.getChildren().stream()
                        .map(mod -> new Context("m." + c.getDep() , mod))
                        .collect(Collectors.toList())));

        return context;
    }

    public static List<Context> getChildSkipModifierFeatures(Token token, List<String> filter) {
        List<Context> context = new ArrayList<>();
        token.getChildren().stream()
                .filter(c -> filter.contains(c.getDep()))
                .forEach(c -> c.getChildren().stream()
                        .forEach(s -> context.addAll(s.getChildren().stream()
                                .map(sc -> new Context(".." + c.getDep(), sc))
                                .collect(Collectors.toList()))));
        return context;
    }

    private static List<Token> getRootPath(Token token) {
        List<Token> rootPath = new ArrayList<>();
        rootPath.add(token);
        while (token.getHead() != null) {
            rootPath.add(token.getHead());
            token = token.getHead();
        }
        return rootPath;
    }

}
