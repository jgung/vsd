package edu.colorado.clear.nlp.feature;

import com.google.common.base.Stopwatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.nlp.type.Token;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class ClusterFeatureExtractor implements FeatureExtractor {

    @Getter
    private String key;
    @Getter
    private boolean lower = true;
    @Getter
    private Map<String, String> clusters;

    public ClusterFeatureExtractor(String key, boolean lower, InputStream clusterFile) {
        this.key = key;
        clusters = new HashMap<>();
        Stopwatch stopwatch = Stopwatch.createStarted();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(clusterFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (lower) {
                    line = line.toLowerCase();
                }
                String[] fields = line.split("\\t");
                String word = fields[0].toLowerCase();
                clusters.put(word, fields[1]);
            }
            log.info("Loaded clusters {} ({})", key, stopwatch.stop());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Feature> extract(Context context) {
        List<Feature> features = new ArrayList<>();
        for (Token token : context.getTokens()) {
            String text = lower ? token.getForm().toLowerCase() : token.getForm();
            String feat = clusters.get(text);
            if (feat == null) {
                text = lower ? token.getLemma().toLowerCase() : token.getLemma();
                feat = clusters.get(text);
            }
            if (feat != null) {
                features.add(new Feature(context, key, feat));
            }
        }
        return features;
    }
}
