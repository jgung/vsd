package edu.colorado.clear.nlp.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public class Token {

    @Getter
    @Setter
    private int index;
    @Getter
    private String lemma;
    @Getter
    private String form;
    @Getter
    private String pos;
    @Getter
    private String dep;
    @Getter
    @Setter
    private Token head;
    @Getter
    @Setter
    private String sense;
    @Getter
    @Setter
    private List<Token> children;

    private Map<String, String> featureMap;

    public Token() {
        children = new ArrayList<>();
        featureMap = new HashMap<>();
    }

    public Token(String form) {
        this();
        setForm(form);
    }

    public List<Token> getAllChildrenInclusive() {
        List<Token> tokens = new ArrayList<>();
        tokens.add(this);
        for (Token child : children) {
            child.getAllChildrenInclusive(tokens);
        }
        tokens.sort((c1, c2) -> Integer.compare(c1.getIndex(), c2.getIndex()));
        return tokens;
    }

    private void getAllChildrenInclusive(List<Token> tokens) {
        tokens.add(this);
        for (Token child : children) {
            child.getAllChildrenInclusive(tokens);
        }
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
        addFeature(Feats.Lemma.getKey(), lemma);
    }


    public void setForm(String form) {
        this.form = form;
        addFeature(Feats.Form.getKey(), form);
    }

    public void setPos(String pos) {
        this.pos = pos;
        addFeature(Feats.Pos.getKey(), pos);
    }

    public void setDep(String dep) {
        this.dep = dep;
        addFeature(Feats.Dep.getKey(), dep);
    }

    public void addFeature(String key, String value) {
        featureMap.put(key, value);
    }

    public String getFeature(String key) {
        return featureMap.get(key);
    }


    public String toStringConll() {
        return index + "\t" + form + "\t" + lemma + "\t" + pos + "\t" + dep + "\t"
                + (head == null ? -1 : head.getIndex()) + "\t" + (sense == null ? "-" : sense);
    }

}
