package edu.colorado.clear.nlp.type;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

/**
 * @author jamesgung
 */
public class VerbNetInstance {

    @Getter
    private String path;
    @Getter
    private int sentenceIndex;
    @Getter
    private int tokenIndex;
    @Getter
    private String lemma;
    @Getter
    private String label;
    @Getter
    private String text;

    public VerbNetInstance(String line) {
        String[] headerText = line.split("\t");
        String[] headerFields = headerText[0].split(" ");
        text = headerText[1];
        path = headerFields[0];
        sentenceIndex = Integer.parseInt(headerFields[1]);
        tokenIndex = Integer.parseInt(headerFields[2]);
        lemma = headerFields[3];
        label = getId(headerFields[4]);
        if (label.contains("-")) {
            label = label.substring(0, label.indexOf("-"));
        }
    }

    public static String getId(String id) {
        String result = id;
        while (result.matches("[a-zA-Z_]+-.*")) {
            result = result.replaceFirst("[a-zA-Z_]+-", "");
        }
        return result;
    }


    public static List<VerbNetInstance> readInstances(InputStream inputStream) {
        List<VerbNetInstance> instances = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.isEmpty()) {
                    continue;
                }
                instances.add(new VerbNetInstance(line.trim()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return instances;
    }

    @Override
    public String toString() {
        return path + " " + sentenceIndex + " " + tokenIndex + " " + lemma + " " + label + "\t" + text;
    }


}
