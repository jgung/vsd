package edu.colorado.clear.nlp.type;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public class VnSentence {

    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private List<Token> tokens;
    @Getter
    @Setter
    private Token root;

    @Getter
    private List<VerbNetInstance> instances;

    public VnSentence(List<Token> tokens) {
        this.tokens = tokens;
    }

    public void setInstances(List<VerbNetInstance> instances) {
        this.instances = instances;
        for (VerbNetInstance instance : instances) {
            tokens.get(instance.getTokenIndex()).setLemma(instance.getLemma());
        }
    }

    public String toStringConll() {
        StringBuilder sb = new StringBuilder();
        if (instances != null) {
            for (VerbNetInstance instance : instances) {
                sb.append("#").append(instance.toString()).append("\n");
            }
        }
        for (Token token : tokens) {
            sb.append(token.toStringConll()).append("\n");
        }
        return sb.toString();
    }

}
