package edu.colorado.clear.nlp.type;

import lombok.Getter;

/**
 * @author jamesgung
 */
public enum Feats {

    Form("F"),
    Lemma("L"),
    Pos("P"),
    Dep("D"),
    Brown("B"),
    WordNet("WN"),
    DDN("DDN");

    @Getter
    private String key;

    Feats(String key) {
        this.key = key;
    }

}
