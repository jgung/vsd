package edu.colorado.clear.nlp.experiment;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import edu.colorado.clear.ml.vector.LabeledVector;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class CrossValidation {

    private Random random;

    public CrossValidation(int seed) {
        random = new Random(seed);
    }

    public CrossValidation() {
        this(0);
    }

    public List<Fold> createFolds(List<LabeledVector> instances, int numFolds, double ratio) {
        ListMultimap<String, LabeledVector> partition = partitionToClasses(instances);
        return sampleFolds(partition, numFolds, ratio);
    }

    private List<Fold> sampleFolds(ListMultimap<String, LabeledVector> partition, int numFolds, double ratio) {
        List<List<Fold>> perClassFolds = partition.keySet().stream().
                map(key -> sampleFolds(partition.get(key), numFolds, ratio))
                .collect(Collectors.toList());
        return combineFolds(perClassFolds, numFolds);
    }

    private List<Fold> combineFolds(List<List<Fold>> folds, int numFolds) {
        List<Fold> combined = new ArrayList<>();
        for (int i = 0; i < numFolds; ++i) {
            Fold fold = new Fold();
            for (List<Fold> subsets : folds) {
                fold.add(subsets.get(i));
            }
            combined.add(fold);
        }
        return combined;
    }

    private List<Fold> sampleFolds(List<LabeledVector> instances, int numSubsets, double ratio) {
        List<Fold> subsets = new ArrayList<>();
        int trainInstances = (int) (ratio * instances.size());
        int testInstances = instances.size() - trainInstances;
        if (testInstances == 0) {
            if (trainInstances > 1) {
                trainInstances--;
                testInstances++;
            } else {
                log.warn("Need at least two instances to create a train/test fold.");
            }
        }
        for (int subset = 0; subset < numSubsets; ++subset) {
            Collections.shuffle(instances, random);
            List<LabeledVector> train = new ArrayList<>();
            for (int i = 0; i < trainInstances; ++i) {
                train.add(instances.get(i));
            }
            List<LabeledVector> test = new ArrayList<>();
            for (int i = trainInstances; i < trainInstances + testInstances; ++i) {
                test.add(instances.get(i));
            }
            subsets.add(new Fold(train, test));
        }
        return subsets;
    }

    private ListMultimap<String, LabeledVector> partitionToClasses(List<LabeledVector> instances) {
        ListMultimap<String, LabeledVector> map = ArrayListMultimap.create();
        for (LabeledVector vector : instances) {
            map.put(vector.getLabel(), vector);
        }
        return map;
    }

    public static class Fold {

        @Getter
        private List<LabeledVector> trainInstances;
        @Getter
        private List<LabeledVector> testInstances;

        public Fold(List<LabeledVector> trainInstances, List<LabeledVector> testInstances) {
            this.trainInstances = trainInstances;
            this.testInstances = testInstances;
        }

        public Fold() {
            this(new ArrayList<>(), new ArrayList<>());
        }

        private void add(Fold fold) {
            trainInstances.addAll(fold.getTrainInstances());
            testInstances.addAll(fold.getTestInstances());
        }

    }

}
