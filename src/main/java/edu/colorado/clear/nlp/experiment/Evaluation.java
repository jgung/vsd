package edu.colorado.clear.nlp.experiment;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Getter;

/**
 * @author jamesgung
 */
public class Evaluation {

    private Multiset<String> systemCounts;
    private Multiset<String> goldCounts;
    private Multiset<String> correctCounts;

    @Getter
    private ConfusionMatrix matrix;

    public Evaluation() {
        this.matrix = new ConfusionMatrix();
        systemCounts = HashMultiset.create();
        goldCounts = HashMultiset.create();
        correctCounts = HashMultiset.create();
    }

    public void add(String system, String gold) {
        add(system, gold, 1);
    }

    public void add(String system, String gold, int count) {
        matrix.add(system, gold, count);
        systemCounts.add(system, count);
        goldCounts.add(gold, count);
        if (system.equals(gold)) {
            correctCounts.add(system, count);
        }
    }

    public void add(Evaluation evaluation) {
        matrix.add(evaluation.getMatrix());
        for (String value : evaluation.systemCounts.elementSet()) {
            systemCounts.add(value, evaluation.systemCounts.count(value));
        }
        for (String value : evaluation.goldCounts.elementSet()) {
            goldCounts.add(value, evaluation.goldCounts.count(value));
        }
        for (String value : evaluation.correctCounts.elementSet()) {
            correctCounts.add(value, evaluation.correctCounts.count(value));
        }
    }

    public int countCorrect(String value) {
        return correctCounts.count(value);
    }

    public int countPredictions(String value) {
        return systemCounts.count(value);
    }

    public int countGold(String value) {
        return goldCounts.count(value);
    }

    public int countCorrect() {
        return correctCounts.size();
    }

    public int countPredictions() {
        return systemCounts.size();
    }

    public int countGold() {
        return goldCounts.size();
    }

    public double precision() {
        int count = countPredictions();
        return count == 0 ? 1.0 : (double) countCorrect() / count;
    }

    public double precision(String value) {
        int count = countPredictions(value);
        return count == 0 ? 1.0 : (double) countCorrect(value) / count;
    }

    public double recall() {
        int count = countGold();
        return count == 0 ? 1.0 : (double) countCorrect() / count;
    }

    public double recall(String value) {
        int count = countGold(value);
        return count == 0 ? 1.0 : (double) countCorrect(value) / count;
    }

    public double fb(double beta) {
        double precision = this.precision();
        double recall = this.recall();
        double numerator = precision * recall * (1d + beta * beta);
        double denominator = (beta * beta * precision) + recall;
        return denominator == 0.0 ? 0.0 : numerator / denominator;
    }

    public double fb(double beta, String outcome) {
        double precision = this.precision(outcome);
        double recall = this.recall(outcome);
        double numerator = (1d + beta * beta) * precision * recall;
        double denominator = (beta * beta * precision) + recall;
        return denominator == 0.0 ? 0.0 : numerator / denominator;
    }

    public double f1() {
        return fb(1d);
    }

    public double f1(String outcome) {
        return fb(1d, outcome);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("P\tR\tF1\t#gold\t#system\t#correct\n");
        sb.append(String.format(
                "%.3f\t%.3f\t%.3f\t%d\t%d\t%d\tOVERALL\n",
                this.precision(),
                this.recall(),
                this.f1(),
                this.goldCounts.size(),
                this.systemCounts.size(),
                this.correctCounts.size()));
        List<String> outcomes = new ArrayList<>(goldCounts.elementSet());
        if (outcomes.size() > 1) {
            Collections.sort(outcomes);
            for (String outcome : outcomes) {
                sb.append(String.format(
                        "%.3f\t%.3f\t%.3f\t%d\t%d\t%d\t%s\n",
                        this.precision(outcome),
                        this.recall(outcome),
                        this.f1(outcome),
                        this.goldCounts.count(outcome),
                        this.systemCounts.count(outcome),
                        this.correctCounts.count(outcome),
                        outcome));
            }
        }
        return sb.toString();
    }

}
