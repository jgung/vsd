package edu.colorado.clear.nlp.experiment.verbnet;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.nlp.application.vsd.VerbNetSingle;
import edu.colorado.clear.nlp.experiment.CrossValidation;
import edu.colorado.clear.nlp.experiment.Evaluation;
import edu.colorado.clear.nlp.parser.ConllDepVnReader;
import edu.colorado.clear.nlp.parser.VnSentenceParser;

/**
 * @author jamesgung
 */
public class VerbNetExperiments {

    public static void crossValidate(int folds, double ratio, String instancePath, String parsePath, String[] args,
                                     VnSentenceParser wrapper) throws FileNotFoundException {
        // parse, if necessary
        if (wrapper != null) {
            ConllDepVnReader.parseAndWriteVerbNetInstances(instancePath, parsePath, wrapper);
        }
        VerbNetSingle verbNetSingle = new VerbNetSingle();
        verbNetSingle.initialize();
        verbNetSingle.initializeArgs(args);
        List<LabeledVector> instances = verbNetSingle.loadInstances(parsePath, true);
        List<CrossValidation.Fold> foldList = new CrossValidation().createFolds(instances, folds, ratio);
        Evaluation total = new Evaluation();
        for (CrossValidation.Fold fold : foldList) {
            verbNetSingle.train(fold.getTrainInstances(), new ArrayList<>());
            Evaluation evaluation = new Evaluation();
            for (LabeledVector vector : fold.getTestInstances()) {
                String result = verbNetSingle.apply(vector);
                evaluation.add(result, vector.getLabel());
            }
            System.out.println(evaluation.toString());
            System.out.println(evaluation.getMatrix().toString());
            total.add(evaluation);
        }
        System.out.println(total.toString());
        System.out.println(total.getMatrix().toString());
    }

}
