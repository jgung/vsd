package edu.colorado.clear.nlp.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import edu.colorado.clear.nlp.type.VnSentence;
import edu.colorado.clear.nlp.type.VerbNetInstance;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class ConllDepVnReader extends ConllDepReader {

    @Override
    public VnSentence readSentence(List<String> lines) {
        int lineIndex = 0;
        List<VerbNetInstance> instances = new ArrayList<>();
        while(lines.get(lineIndex).startsWith("#")) {
            instances.add(new VerbNetInstance(lines.get(lineIndex++).substring(1)));
        }
        VnSentence sentence = super.readSentence(lines.subList(lineIndex, lines.size()));
        sentence.setInstances(instances);
        return sentence;
    }

    public static void parseAndWriteVerbNetInstances(String vnPath, String outPath, VnSentenceParser wrapper) throws
            FileNotFoundException {
        List<VerbNetInstance> instances = VerbNetInstance.readInstances(new FileInputStream(vnPath));
        List<VnSentence> sentences = new ArrayList<>();
        int count = 0;
        log.info("Reading instances...");
        for (VerbNetInstance instance : instances) {
            if (count % 250 == 0) {
                log.info(String.format("%.2f%%", 100.0 * count / instances.size()));
            }
            VnSentence sentence = wrapper.process(Arrays.asList(instance.getText().split(" ")));
            sentence.setInstances(Collections.singletonList(instance));
            sentence.getTokens().get(instance.getTokenIndex()).setSense(instance.getLabel());
            sentences.add(sentence);
            ++count;
        }
        writeConllInstances(sentences, new File(outPath));
    }

    public static void main(String... args) throws Throwable {
        parseAndWriteVerbNetInstances("data/datasets/clean/see.txt", "data/datasets/clean/see.dep",
                new StanfordWrapper(true));
    }
}
