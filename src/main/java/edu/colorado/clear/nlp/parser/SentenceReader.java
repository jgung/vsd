package edu.colorado.clear.nlp.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import edu.colorado.clear.nlp.type.VnSentence;

/**
 * @author jamesgung
 */
public abstract class SentenceReader {

    public List<VnSentence> readSentences(File file) {
        List<VnSentence> sentences = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            List<String> lines = new ArrayList<>();
            while((line = reader.readLine()) != null) {
                if (line.length() == 0) {
                    if (lines.size() > 0) {
                        sentences.add(readSentence(lines));
                        lines = new ArrayList<>();
                    }
                    continue;
                }
                lines.add(line);
            }
            if (lines.size() > 0) {
                sentences.add(readSentence(lines));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sentences;
    }

    public static void writeConllInstances(List<VnSentence> instances, File path) {
        try (PrintWriter writer = new PrintWriter(path)) {
            for (VnSentence sentence : instances) {
                writer.println(sentence.toStringConll());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public abstract VnSentence readSentence(List<String> lines);

}
