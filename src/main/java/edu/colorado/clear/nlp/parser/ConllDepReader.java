package edu.colorado.clear.nlp.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.colorado.clear.nlp.type.VnSentence;
import edu.colorado.clear.nlp.type.Token;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class ConllDepReader extends SentenceReader {

    @Override
    public VnSentence readSentence(List<String> lines) {
        List<Token> tokens = new ArrayList<>();
        Map<Integer, Integer> headMap = new HashMap<>();
        int lineIndex = 0;
        int tokenIndex = 0;
        for (; lineIndex < lines.size(); ++lineIndex) {
            String line = lines.get(lineIndex);
            String[] fields = line.split("\t");
            Token token = new Token();
            token.setIndex(tokenIndex);
            token.setForm(fields[1]);
            token.setLemma(fields[2]);
            token.setPos(fields[3]);
            token.setDep(fields[4]);
            headMap.put(tokenIndex++, Integer.parseInt(fields[5]));
            token.setSense(fields[6]);
            tokens.add(token);
        }
        Token root = null;
        for (Map.Entry<Integer, Integer> entry : headMap.entrySet()) {
            Token token = tokens.get(entry.getKey());
            if (entry.getValue() >= 0) {
                token.setHead(tokens.get(entry.getValue()));
                tokens.get(entry.getValue()).getChildren().add(token);
            } else {
                root = token;
            }
        }
        VnSentence sentence = new VnSentence(tokens);
        sentence.setRoot(root);
        return sentence;
    }

}
