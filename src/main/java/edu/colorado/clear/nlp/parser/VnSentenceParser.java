package edu.colorado.clear.nlp.parser;

import java.util.List;

import edu.colorado.clear.nlp.type.VnSentence;

/**
 * @author jamesgung
 */
public interface VnSentenceParser {

    VnSentence process(List<String> tokens);

    List<String> tokenize(String string);

}
