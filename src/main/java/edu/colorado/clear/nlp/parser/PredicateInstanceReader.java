package edu.colorado.clear.nlp.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import edu.colorado.clear.nlp.type.VnSentence;
import edu.colorado.clear.nlp.type.Token;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class PredicateInstanceReader extends SentenceReader {

    @Override
    public VnSentence readSentence(List<String> lines) {
        List<Token> tokens = new ArrayList<>();
        int tokenIndex = 0;
        for (String line : lines) {
            String[] fields = line.split("\t");
            Token token = new Token();
            token.setIndex(tokenIndex);
            token.setForm(fields[0]);
            token.setSense(fields[1]);
            tokenIndex++;
            tokens.add(token);
        }
        return new VnSentence(tokens);
    }

    public void parseAndWriteInstances(String instancePath, String outPath, VnSentenceParser wrapper) throws
            FileNotFoundException {
        List<VnSentence> unparsed = this.readSentences(new File(instancePath));
        List<VnSentence> sentences = new ArrayList<>();
        int count = 0;
        log.info("Reading instances...");
        for (VnSentence instance : unparsed) {
            if (count % 250 == 0) {
                log.info(String.format("%.2f%%", 100.0 * count / unparsed.size()));
            }
            VnSentence parsed = wrapper.process(instance.getTokens().stream().map(Token::getForm).collect(Collectors.toList()));
            int i = 0;
            for (Token token: instance.getTokens()) {
                Token parsedToken = parsed.getTokens().get(i++);
                parsedToken.setSense(token.getSense());
            }
            sentences.add(parsed);
            ++count;
        }
        writeConllInstances(sentences, new File(outPath));
    }

    public static void main(String[] args) throws Throwable {
        new PredicateInstanceReader()
                .parseAndWriteInstances("data/datasets/conll2012/train-preds.txt", "data/datasets/conll2012/dep/train-preds.dep",
                new StanfordWrapper(true));
    }


}
