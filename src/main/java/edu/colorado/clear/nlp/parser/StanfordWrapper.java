package edu.colorado.clear.nlp.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.colorado.clear.nlp.type.VnSentence;
import edu.colorado.clear.nlp.type.Token;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.nndep.DependencyParser;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.TypedDependency;

/**
 * @author jamesgung
 */
public class StanfordWrapper implements VnSentenceParser {

    private static final String NO_ESCAPING = "ptb3Escaping=false";

    private TokenizerFactory tf;
    private MaxentTagger tagger;
    private Morphology lemmatizer;
    private DependencyParser parser;

    public StanfordWrapper(boolean loadParser) {
        tf = PTBTokenizer.coreLabelFactory();
        if (loadParser) {
            tagger = new MaxentTagger(MaxentTagger.DEFAULT_JAR_PATH);
            lemmatizer = new Morphology();
            parser = DependencyParser.loadFromModelFile(DependencyParser.DEFAULT_MODEL);
        }
    }

    private List<CoreLabel> getStanfordTokens(List<String> tokens) {
        List<CoreLabel> cls = new ArrayList<>();
        for (String token : tokens) {
            CoreLabel cl = new CoreLabel();
            cl.setOriginalText(token);
            cl.setWord(token);
            cl.setValue(token);
            cls.add(cl);
        }
        return cls;
    }

    private List<CoreLabel> tag(List<String> tokens) {
        List<CoreLabel> cls = getStanfordTokens(tokens);
        tagger.tagCoreLabels(cls);
        return lemmatize(cls);
    }

    private List<CoreLabel> lemmatize(List<CoreLabel> words) {
        for (CoreLabel word : words) {
            word.setLemma(lemmatizer.lemma(word.word(), word.tag()));
        }
        return words;
    }

    private List<Token> toTokens(List<CoreLabel> cls) {
        int index = 0;
        List<Token> tokens = new ArrayList<>();
        for (Iterator<CoreLabel> iterator = cls.iterator(); iterator.hasNext(); ++index) {
            CoreLabel coreLabel = iterator.next();
            Token token = new Token(coreLabel.word());
            token.setLemma(coreLabel.lemma());
            token.setIndex(index);
            token.setPos(coreLabel.tag());
            tokens.add(token);
        }
        return tokens;
    }

    private VnSentence parse(List<CoreLabel> cls) {
        List<Token> tokens = toTokens(cls);
        Collection<TypedDependency> dependencies = parser.predict(cls).typedDependencies();
        Map<Integer, TypedDependency> dependencyMap = new HashMap<>();
        for (TypedDependency dep : dependencies) {
            dependencyMap.put(dep.dep().index(), dep);
        }
        Token root = null;
        for (Token token : tokens) {
            int head = dependencyMap.get(token.getIndex() + 1).gov().index() - 1;
            if (head >= 0) {
                token.setHead(tokens.get(head));
                tokens.get(head).getChildren().add(token);
            } else {
                root = token;
            }
            TypedDependency parent = dependencyMap.get(token.getIndex() + 1);
            token.setDep(parent.reln().toString());
        }

        VnSentence sentence = new VnSentence(tokens);
        sentence.setRoot(root);
        return sentence;
    }

    @Override
    public VnSentence process(List<String> tokens) {
        return parse(tag(tokens));
    }

    @Override
    public List<String> tokenize(String text) {
        Tokenizer tokenizer = tf.getTokenizer(new StringReader(text), NO_ESCAPING);
        List<String> tokens = new ArrayList<>();
        while (tokenizer.hasNext()) {
            tokens.add(((HasWord) tokenizer.next()).word());
        }
        return tokens;
    }

}
