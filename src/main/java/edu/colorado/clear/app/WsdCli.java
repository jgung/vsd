package edu.colorado.clear.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.nlp.application.vsd.VerbNetSingle;
import edu.colorado.clear.nlp.experiment.Evaluation;
import edu.colorado.clear.nlp.experiment.verbnet.VerbNetExperiments;
import edu.colorado.clear.nlp.parser.ConllDepVnReader;
import edu.colorado.clear.nlp.parser.StanfordWrapper;
import edu.colorado.clear.nlp.parser.VnSentenceParser;
import edu.colorado.clear.nlp.type.VnSentence;

/**
 * @author jamesgung
 */
public class WsdCli {

    @Parameter(names = "-data", description = "path to the classifier data directory")
    private String dataPath = "edu/colorado/clear/nlp/models/feature-extractors.model";
    @Parameter(names = "-model", description = "path to the classifier model", required = true)
    private String model;

    @Parameter(names = "--train", description = "train a new model on the given data")
    private Boolean train = false;
    @Parameter(names = "--skipParse", description = "skip the parsing step on the data")
    private Boolean skipParse = false;
    @Parameter(names = "-train", description = "path to training instances")
    private String trainPath;
    @Parameter(names = "-valid", description = "path to validation instances")
    private String validPath;
    @Parameter(names = "-test", description = "path to testing instances")
    private String testPath;

    @Parameter(names = "--cv", description = "perform sampled, stratified cross validation on the training data")
    private Boolean cv = false;
    @Parameter(names = "-folds", description = "number of cross validation folds to sample")
    private Integer folds = 10;
    @Parameter(names = "-per", description = "percentage of instances to use for training in each fold")
    private Double trainPer = 0.8;

    @Parameter(names = "--test", description = "test on the testing data")
    private Boolean test = false;

    @Parameter(names = "--apply", description = "apply classifier to test data and display results")
    private Boolean apply = false;
    @Parameter(names = "--errOnly", description = "only display erroneous classifications")
    private Boolean errOnly = true;

    @Parameter(names = "--itl", description = "engage interactive loop for testing")
    private Boolean itl = false;

    private VnSentenceParser wrapper;

    public WsdCli(String... args) {
        JCommander jCommander = new JCommander(this);
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            jCommander.usage();
            System.exit(0);
        }
    }

    public void run() throws FileNotFoundException {
        if (!(train || cv || itl || test || apply)) {
            System.out.println("Must choose one of --train, --test, --apply, --cv, or --itl");
            System.exit(0);
        }
        if (!skipParse || itl) {
            wrapper = new StanfordWrapper(true);
        }
        if (cv || train) {
            if (trainPath == null) {
                System.out.println("Must supply a training data file path (e.g. \"-train data.txt\").");
            } else if (!new File(trainPath).exists()) {
                System.out.println("Unable to locate training file: " + new File(trainPath).getAbsolutePath());
            } else {
                if (train) {
                    train();
                } else if (cv) {
                    cv();
                }
            }
        }
        if (test || apply) {
            if (testPath == null) {
                System.out.println("Test path must be provided when --test option is active");
            } else {
                if (test) {
                    test();
                }
                if (apply) {
                    apply();
                }
            }
        }
        if (itl) {
            interactive();
        }
    }

    private void parseInstances(String instances, String parsePath) throws FileNotFoundException {
        ConllDepVnReader.parseAndWriteVerbNetInstances(instances, parsePath, wrapper);
    }

    private void train() throws FileNotFoundException {
        if (!skipParse) {
            parseInstances(trainPath, trainPath = trainPath + ".dep");
            if (validPath != null) {
                parseInstances(validPath, validPath = validPath + ".dep");
            }
        }
        VerbNetSingle vsd = new VerbNetSingle();
        vsd.initialize();
        vsd.initializeArgs("-data", dataPath);
        List<LabeledVector> trainInstances = vsd.loadInstances(trainPath, true);
        List<LabeledVector> validInstances = validPath == null ? null : vsd.loadInstances(validPath, false);
        vsd.train(trainInstances, validInstances == null ? new ArrayList<>() : validInstances);
        vsd.saveModel(new FileOutputStream(model));
    }

    private VerbNetSingle loadClassifier() throws FileNotFoundException {
        VerbNetSingle vsd = new VerbNetSingle();
        vsd.initializeArgs("-data", dataPath);
        vsd.loadModel(new FileInputStream(model));
        return vsd;
    }

    private void apply() throws FileNotFoundException {
        if (!skipParse) {
            parseInstances(testPath, testPath = testPath + ".dep");
        }
        VerbNetSingle vsd = loadClassifier();
        List<LabeledVector> testInstances = vsd.loadInstances(testPath, false);
        List<VnSentence> sentences = new ConllDepVnReader().readSentences(new File(testPath));
        int index = 0;
        for (LabeledVector vector : testInstances) {
            String result = vsd.apply(vector);
            if (!errOnly || !result.equals(vector.getLabel())) {
                System.out.println(result + "\t"
                        + sentences.get(index++).getInstances().get(0).toString());
            }
        }
    }

    private void test() throws FileNotFoundException {
        if (!skipParse) {
            parseInstances(testPath, testPath = testPath + ".dep");
        }
        VerbNetSingle vsd = loadClassifier();
        List<LabeledVector> testInstances = vsd.loadInstances(testPath, false);
        Evaluation evaluation = new Evaluation();
        for (LabeledVector vector : testInstances) {
            String result = vsd.apply(vector);
            evaluation.add(result, vector.getLabel());
        }
        System.out.println(evaluation.toString());
        System.out.println(evaluation.getMatrix().toString());
    }

    private void cv() throws FileNotFoundException {
        VerbNetExperiments.crossValidate(folds, trainPer, trainPath,
                trainPath + ".dep", new String[]{"-data", dataPath}, wrapper);
    }

    private void interactive() {
        VerbNetClassifier classifier = new VerbNetClassifier(model, wrapper);
        classifier.interactiveLoop();
    }

    public static void main(String[] args) throws FileNotFoundException {
        WsdCli classifier = new WsdCli(args);
        classifier.run();
    }

}
