package edu.colorado.clear.app;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import edu.colorado.clear.nlp.application.StringParser;
import edu.colorado.clear.nlp.application.TokenLabeler;
import edu.colorado.clear.nlp.application.pred.PredicateClassifier;
import edu.colorado.clear.nlp.parser.VnSentenceParser;
import edu.colorado.clear.nlp.parser.StanfordWrapper;
import edu.colorado.clear.nlp.type.Token;
import edu.colorado.clear.nlp.type.VnSentence;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class PredicateIdentifier implements TokenLabeler, StringParser {

    @Override
    public List<String> label(List<String> tokens) {
        VnSentence sentence = parser.process(tokens);
        predId.process(sentence);
        return sentence.getTokens().stream().map(Token::getSense).collect(Collectors.toList());
    }

    @Getter
    private VnSentenceParser parser;
    @Getter
    private PredicateClassifier predId;

    public PredicateIdentifier(String path) {
        this(path, new StanfordWrapper(true));
    }

    public PredicateIdentifier(String path, VnSentenceParser wrapper) {
        parser = wrapper;
        predId = new PredicateClassifier();
        try {
            predId.loadModel(new FileInputStream(path));
        } catch (IOException e) {
            log.error("Error loading model at {}", path, e);
            throw new RuntimeException(e);
        }
    }


    @Override
    public VnSentence parse(String input) {
        VnSentence sentence = parser.process(parser.tokenize(input));
        predId.process(sentence);
        return sentence;
    }

    public void interactiveLoop() {
        System.out.println("Enter a sentence (\"EXIT\" to exit.)");
        Scanner scanner = new Scanner(System.in);
        String input;
        while(true) {
            System.out.print("> ");
            input = scanner.nextLine().trim();
            if (input.equals("EXIT")) {
                break;
            }
            System.out.println(parse(input).toStringConll());
        }
    }

    public static void main(String... args) {
        PredicateIdentifier classifier = new PredicateIdentifier("data/models/pred.model");
        classifier.interactiveLoop();
    }


}
