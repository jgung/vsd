package edu.colorado.clear.app;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import edu.colorado.clear.nlp.application.SentenceProcessor;
import edu.colorado.clear.nlp.application.StringParser;
import edu.colorado.clear.nlp.application.vsd.VerbNetSingle;
import edu.colorado.clear.nlp.parser.VnSentenceParser;
import edu.colorado.clear.nlp.parser.StanfordWrapper;
import edu.colorado.clear.nlp.type.VnSentence;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class VerbNetClassifier implements SentenceProcessor, StringParser {

    @Getter
    private VnSentenceParser parser;
    @Getter
    private VerbNetSingle vsd;

    public VerbNetClassifier(String path) {
        this(path, new StanfordWrapper(true));
    }

    public VerbNetClassifier(String path, VnSentenceParser wrapper) {
        parser = wrapper;
        vsd = new VerbNetSingle();
        vsd.initializeArgs();
        try {
            vsd.loadModel(new FileInputStream(path));
        } catch (IOException e) {
            log.error("Error loading model at {}", path, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void process(VnSentence input) {
        vsd.process(input);
    }

    @Override
    public VnSentence parse(String input) {
        VnSentence sentence = parser.process(parser.tokenize(input));
        process(sentence);
        return sentence;
    }

    public void interactiveLoop() {
        System.out.println("Enter a sentence (\"QUIT\" to exit.)");
        Scanner scanner = new Scanner(System.in);
        String input;
        while(true) {
            System.out.print("> ");
            input = scanner.nextLine().trim();
            if (input.equals("QUIT")) {
                break;
            }
            System.out.println(parse(input).toStringConll());
        }
    }

    public static void main(String... args) {
        VerbNetClassifier classifier = new VerbNetClassifier("data/models/vn.model");
        classifier.interactiveLoop();
    }

}
