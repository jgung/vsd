package edu.colorado.clear.ml.vector;

import java.io.Serializable;

/**
 * @author jamesgung
 */
public interface Vector extends Serializable {

    float[] getValues();

}
