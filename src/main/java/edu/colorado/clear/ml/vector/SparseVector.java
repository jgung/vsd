package edu.colorado.clear.ml.vector;

import java.util.List;

import edu.colorado.clear.util.Pair;
import lombok.Getter;

/**
 * @author jamesgung
 */
public class SparseVector implements Vector {

    @Getter
    private int[] indices;
    @Getter
    private float[] values;

    public SparseVector(List<Pair<Integer, Float>> tuples) {
        tuples.sort((t1, t2) -> Integer.compare(t1.getFirst(), t2.getFirst()));
        this.indices = new int[tuples.size()];
        this.values = new float[tuples.size()];
        for (int i = 0; i < indices.length; ++i) {
            Pair<Integer, Float> pair = tuples.get(i);
            this.indices[i] = pair.getFirst();
            this.values[i] = pair.getSecond();
        }
    }

    public float dot(float[] dense) {
        float sum = 0;
        for (int i = 0; i < indices.length; ++i) {
            sum += values[i] * dense[indices[i]];
        }
        return sum;
    }

    public float l2() {
        float sum = 0;
        for (int i = 0; i < indices.length; ++i) {
            sum += values[i] * values[i];
        }
        return (float) Math.sqrt(sum);
    }

    public void addToDense(double weight, float[] dense) {
        for (int i = 0; i < indices.length; ++i) {
            dense[indices[i]] += values[i] * weight;
        }
    }

}
