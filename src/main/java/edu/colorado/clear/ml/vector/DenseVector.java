package edu.colorado.clear.ml.vector;

import lombok.Getter;

/**
 * @author jamesgung
 */
public class DenseVector implements Vector {

    @Getter
    protected float[] values;

    public DenseVector(float[] values) {
        this.values = values;
    }

}
