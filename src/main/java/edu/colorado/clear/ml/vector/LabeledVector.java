package edu.colorado.clear.ml.vector;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public class LabeledVector implements Vector {

    @Getter
    @Setter
    private String id;
    @Getter
    private String label;
    @Getter
    private Vector vector;

    @Override
    public float[] getValues() {
        return vector.getValues();
    }

    public LabeledVector(String label, Vector vector) {
        this.label = label;
        this.vector = vector;
    }
}
