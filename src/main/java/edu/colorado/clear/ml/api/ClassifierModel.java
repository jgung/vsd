package edu.colorado.clear.ml.api;

import com.google.common.collect.BiMap;

import java.io.Serializable;

/**
 * @author jamesgung
 */
public interface ClassifierModel extends Serializable {

    BiMap<String, Integer> getFeatureMap();

    BiMap<String, Integer> getLabelMap();

}
