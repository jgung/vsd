package edu.colorado.clear.ml.api;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import edu.colorado.clear.ml.vector.LabeledVector;

/**
 * @author jamesgung
 */
public interface Classifier<T extends ClassifierModel> {

    void initialize();

    void initialize(T model);

    void train(List<LabeledVector> train, List<LabeledVector> validation);

    float test(List<LabeledVector> test);

    String apply(LabeledVector instance);

    Map<String, Float> score(LabeledVector instance);

    void saveModel(OutputStream outputStream);

    void loadModel(InputStream inputStream);

    void loadOptions(Map<String, Object> hyperparams);

}
