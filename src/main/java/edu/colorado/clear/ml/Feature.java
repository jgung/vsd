package edu.colorado.clear.ml;

import java.util.List;

import edu.colorado.clear.nlp.feature.Context;
import lombok.Getter;


/**
 * @author jamesgung
 */
public class Feature {

    @Getter
    private String id;
    @Getter
    private float value;

    public Feature(String id, float value) {
        this.id = id;
        this.value = value;
    }

    public Feature(String id) {
        this(id, 1f);
    }

    public Feature(Context context, String id, String value) {
        this(context.getLabel() + "." + id + "=" + value);
    }

    public Feature(Context context, String id, List<String> values) {
        this(context.getLabel() + "." + id + "=" + String.join("|", values));
    }

}
