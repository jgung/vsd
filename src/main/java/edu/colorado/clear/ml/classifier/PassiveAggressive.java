package edu.colorado.clear.ml.classifier;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.colorado.clear.ml.vector.DenseVector;
import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.ml.vector.SparseVector;
import edu.colorado.clear.util.Pair;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class PassiveAggressive extends BaseClassifier<ParameterizedModel> {

    @Getter
    @Setter
    private boolean averaging;
    @Getter
    @Setter
    private float cost;
    @Getter
    @Setter
    private int maxEpochs;
    @Getter
    @Setter
    private int patience;
    @Getter
    @Setter
    private boolean shuffle;
    @Getter
    @Setter
    private int seed;
    @Getter
    @Setter
    private boolean verbose;

    private Map<String, LabeledVector> cached;
    private Map<String, LabeledVector> parameters;

    @Override
    public void loadOptions(Map<String, Object> hyperparams) {
        for (Map.Entry<String, Object> entry : hyperparams.entrySet()) {
            try {
                switch (entry.getKey()) {
                    case "averaging":
                        averaging = (boolean) entry.getValue();
                        break;
                    case "cost":
                        cost = (float) entry.getValue();
                        break;
                    case "maxEpochs":
                        maxEpochs = (int) entry.getValue();
                        break;
                    case "patience":
                        patience = (int) entry.getValue();
                        break;
                    case "seed":
                        seed = (int) entry.getValue();
                        break;
                    case "shuffle":
                        shuffle = (boolean) entry.getValue();
                        break;
                    case "verbose":
                        verbose = (boolean) entry.getValue();
                        break;
                    default:
                        log.warn("Unrecognized option: {}", entry.getKey());
                }
            } catch (Exception e) {
                log.warn("Unexpected value for field {}: {}", entry.getKey(), entry.getValue(), e);
            }
        }
    }

    @Override
    public void initialize() {
        super.initialize();
        averaging = true;
        cost = 0.0025f;
        maxEpochs = 999;
        patience = 10;
        seed = 1;
        shuffle = true;
        verbose = true;
    }

    @Override
    public void initialize(ParameterizedModel model) {
        super.initialize(model);
        this.parameters = model.getParameters();
    }

    @Override
    public ParameterizedModel initializeModel() {
        return new ParameterizedModel();
    }

    private void initializeParameters(List<LabeledVector> instances) {
        cached = new HashMap<>();
        parameters = new HashMap<>();
        model.setParameters(parameters);
        int numFeats = model.getFeatureMap().size();
        for (LabeledVector instance : instances) {
            LabeledVector params = parameters.get(instance.getLabel());
            if (params == null) {
                params = new LabeledVector(instance.getLabel(), new DenseVector(new float[numFeats]));
                parameters.put(instance.getLabel(), params);
                if (averaging) {
                    LabeledVector cachedParams = new LabeledVector(instance.getLabel(),
                            new DenseVector(new float[numFeats]));
                    cached.put(instance.getLabel(), cachedParams);
                }
            }
        }
    }

    @Override
    public void train(List<LabeledVector> train, List<LabeledVector> validation) {
        initializeParameters(train);
        Random random = new Random(seed);
        log.info("Beginning training ({} train instances, {} validation instances)", train.size(), validation.size());
        if (parameters.size() > 1) {
            int stopCount = 0;
            int count = 1;
            int min = Integer.MAX_VALUE;
            float validMax = -Float.MAX_VALUE;
            for (int i = 0; i < maxEpochs && stopCount < patience; ++i) {
                if (shuffle) {
                    Collections.shuffle(train, random);
                }
                int incorrect = 0;
                for (LabeledVector instance : train) {
                    if (update(instance, count)) {
                        ++incorrect;
                    }
                }
                if (verbose) {
                    if (verbose) {
                        log.info("Epoch {}: {} / {} incorrect.", i, incorrect, train.size());
                    }
                }

                if (validation.size() > 0) {
                    float validScore = test(validation);
                    if (validScore > validMax) {
                        stopCount = 0;
                        validMax = validScore;
                    } else {
                        ++stopCount;
                    }
                    if (verbose) {
                        log.info("Epoch {} score on development data: {}", i, validScore);
                    }
                } else {
                    if (incorrect < min) {
                        min = incorrect;
                        stopCount = 0;
                    } else {
                        ++stopCount;
                    }
                }
                if (verbose) {
                    log.info("{} epochs remaining.", Math.min(patience - stopCount, maxEpochs));
                }
                ++count;
            }
            if (averaging) {
                averageParameters(count);
            }
        }
    }

    private float score(SparseVector featureVector, LabeledVector weights) {
        return featureVector.dot(weights.getValues());
    }

    private boolean update(LabeledVector instance, int count) {
        LabeledVector correctVec = parameters.get(instance.getLabel());
        double cScore = score((SparseVector) instance.getVector(), correctVec);

        Pair<LabeledVector, Double> result = getMaxIncorrect(instance, correctVec);
        LabeledVector closestIncorrectVec = result.getFirst();
        double maxScore = result.getSecond();

        SparseVector vector = (SparseVector) instance.getVector();
        double loss = Math.max(0, 1 - cScore + maxScore);
        if (loss > 0) {
            update(loss, vector, correctVec, closestIncorrectVec, count);
        }
        return (cScore < maxScore);
    }

    private Pair<LabeledVector, Double> getMaxIncorrect(LabeledVector instance, LabeledVector correctVec) {
        double maxScore = -Double.MAX_VALUE;
        LabeledVector closestIncorrectVec = null;
        for (LabeledVector weights : parameters.values()) {
            if (correctVec == weights) {
                continue;
            }
            double score = score((SparseVector) instance.getVector(), weights);
            if (score > maxScore) {
                maxScore = score;
                closestIncorrectVec = weights;
            }
        }
        if (closestIncorrectVec == null) {
            throw new RuntimeException("Unexpected classifier state.");
        }
        return new Pair<>(closestIncorrectVec, maxScore);
    }

    private void update(double loss, SparseVector vector, LabeledVector pos, LabeledVector neg, int count) {
        double norm = vector.l2();
        double tau = loss / (2 * norm * norm);
        tau = Math.min(cost, tau);
        vector.addToDense(tau, pos.getVector().getValues());
        vector.addToDense(-tau, neg.getVector().getValues());
        if (averaging) {
            vector.addToDense(tau * count, cached.get(pos.getLabel()).getVector().getValues());
            vector.addToDense(-tau * count, cached.get(neg.getLabel()).getVector().getValues());
        }
    }

    private LabeledVector getMax(SparseVector featureVector) {
        double max = -Double.MAX_VALUE;
        LabeledVector maxParams = null;
        for (LabeledVector weights : parameters.values()) {
            double score = score(featureVector, weights);
            if (score > max) {
                max = score;
                maxParams = weights;
            }
        }
        return maxParams;
    }

    private void averageParameters(int count) {
        for (String label : parameters.keySet()) {
            float[] params = parameters.get(label).getValues();
            float[] cachedVector = cached.get(label).getValues();
            for (int i = 0; i < params.length; ++i) {
                params[i] = (params[i] * count - cachedVector[i]) / (count - 1);
            }
        }
    }

    @Override
    public String apply(LabeledVector instance) {
        return getMax((SparseVector) instance.getVector()).getLabel();
    }

    @Override
    public Map<String, Float> score(LabeledVector instance) {
        Map<String, Float> scores = new HashMap<>();
        for (LabeledVector weights : parameters.values()) {
            scores.put(weights.getLabel(), score((SparseVector) instance.getVector(), weights));
        }
        return scores;
    }

}
