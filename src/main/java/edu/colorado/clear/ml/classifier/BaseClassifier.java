package edu.colorado.clear.ml.classifier;

import com.google.common.collect.BiMap;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.ml.vector.SparseVector;
import edu.colorado.clear.ml.api.Classifier;
import edu.colorado.clear.ml.api.ClassifierModel;
import edu.colorado.clear.util.Pair;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public abstract class BaseClassifier<T extends ClassifierModel> implements Classifier<T> {

    @Getter
    protected T model;

    private static final int BIAS = 0; // bias index in feature map

    @Override
    public void initialize() {
        model = initializeModel();
    }

    /**
     * Returns a new initialized instance of the appropriate model.
     *
     * @return model instance
     */
    public abstract T initializeModel();

    @Override
    public void initialize(T model) {
        this.model = model;
    }

    /**
     * Produces a sparse vector representation of a list of features. During training, new features are added to the
     * feature map. Additionally, adds new labels to label-index map during training.
     *
     * @param features list of features
     * @param label    gold label for training
     * @param training indicates whether or not to add new features/labels
     * @return sparse labeled vector
     */
    public LabeledVector createInstance(List<Feature> features, String label, boolean training) {
        BiMap<String, Integer> labelMap = model.getLabelMap();
        BiMap<String, Integer> featureMap = model.getFeatureMap();
        Set<Integer> used = new HashSet<>(); // used to avoid adding duplicate features
        List<Pair<Integer, Float>> tuples = new ArrayList<>();
        tuples.add(new Pair<>(BIAS, 1f)); // add bias node
        for (Feature feature : features) {
            Integer index;
            if (training) {
                labelMap.putIfAbsent(label, labelMap.size());
                index = featureMap.putIfAbsent(feature.getId(), featureMap.size());
                if (index == null) {
                    index = featureMap.size() - 1;
                }
            } else {
                index = featureMap.get(feature.getId());
            }
            if (index == null) {
                continue;
            }
            if (!used.contains(index)) { // avoid adding duplicate feature
                used.add(index);
                tuples.add(new Pair<>(index, feature.getValue()));
            }
        }
        return new LabeledVector(label, new SparseVector(tuples));
    }

    @Override
    public abstract void train(List<LabeledVector> train, List<LabeledVector> validation);

    @Override
    public float test(List<LabeledVector> test) {
        int correct = 0;
        for (LabeledVector vector : test) {
            String prediction = apply(vector);
            if (prediction.equals(vector.getLabel())) {
                correct++;
            }
        }
        return (float) correct / test.size();
    }

    @Override
    public abstract String apply(LabeledVector instance);

    @Override
    public abstract Map<String, Float> score(LabeledVector instance);

    @Override
    public void saveModel(OutputStream outputStream) {
        try (ObjectOutputStream oos = new ObjectOutputStream(outputStream)) {
            oos.writeObject(model);
        } catch (IOException e) {
            log.info("Error saving classifier model.", e);
        }
    }

    @Override
    public void loadModel(InputStream inputStream) {
        try (ObjectInputStream ois = new ObjectInputStream(inputStream)) {
            //noinspection unchecked
            model = (T) ois.readObject();
            initialize(model);
        } catch (IOException | ClassNotFoundException e) {
            log.info("Error loading classifier model.", e);
        }
    }

    @Override
    public abstract void loadOptions(Map<String, Object> hyperparams);

}
