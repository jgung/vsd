package edu.colorado.clear.ml.classifier;

import com.google.common.collect.BiMap;

import edu.colorado.clear.ml.api.ClassifierModel;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public class ClassifierWrapperModel<T extends ClassifierModel> implements ClassifierModel {

    @Getter
    @Setter
    private T wrappedModel;

    @Override
    public BiMap<String, Integer> getFeatureMap() {
        return wrappedModel.getFeatureMap();
    }

    @Override
    public BiMap<String, Integer> getLabelMap() {
        return wrappedModel.getLabelMap();
    }

}
