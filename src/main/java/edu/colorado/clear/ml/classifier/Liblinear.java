package edu.colorado.clear.ml.classifier;

import com.google.common.collect.BiMap;

import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.ml.vector.SparseVector;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


/**
 * @author jamesgung
 */
@Slf4j
public class Liblinear extends BaseClassifier<Liblinear.LiblinearModel> {

    @Getter
    private Model llModel;

    @Getter
    @Setter
    private SolverType solverType;
    @Getter
    @Setter
    private double cost;
    @Getter
    @Setter
    private double eps;

    @Override
    public void initialize() {
        super.initialize();
        solverType = SolverType.L2R_L2LOSS_SVC;
        cost = 1.0d;
        eps = 0.01d;
    }

    @Override
    public void initialize(Liblinear.LiblinearModel model) {
        super.initialize(model);
        llModel = model.getModel();
    }

    @Override
    public Liblinear.LiblinearModel initializeModel() {
        return new LiblinearModel();
    }

    @Override
    public void loadOptions(Map<String, Object> hyperparams) {
        for (Map.Entry<String, Object> entry : hyperparams.entrySet()) {
            try {
                switch (entry.getKey()) {
                    case "solverType":
                        solverType = (SolverType) entry.getValue();
                        break;
                    case "cost":
                        cost = (double) entry.getValue();
                        break;
                    case "eps":
                        eps = (double) entry.getValue();
                        break;
                    default:
                        log.warn("Unrecognized option: {}", entry.getKey());
                }
            } catch (Exception e) {
                log.warn("Unexpected value for field {}: {}", entry.getKey(), entry.getValue(), e);
            }
        }
    }

    @Override
    public void train(List<LabeledVector> train, List<LabeledVector> validation) {
        Problem problem = new Problem();
        problem.l = train.size();
        problem.n = model.getFeatureMap().size();
        problem.x = getFeatures(train);
        problem.y = getLabels(train);
        Parameter parameter = new Parameter(solverType, cost, eps);
        llModel = Linear.train(problem, parameter);
        model.setModel(llModel);
    }

    private double[] getLabels(List<LabeledVector> vectors) {
        BiMap<String, Integer> labelMap = model.getLabelMap();
        return vectors.stream().mapToDouble(l -> labelMap.get(l.getLabel())).toArray();
    }

    private Feature[][] getFeatures(List<LabeledVector> vectors) {
        Feature[][] features = new FeatureNode[vectors.size()][];
        int index = 0;
        for (LabeledVector vector : vectors) {
            features[index++] = getFeatureArray(vector);
        }
        return features;
    }

    private Feature[] getFeatureArray(LabeledVector vector) {
        SparseVector sparse = (SparseVector) vector.getVector();
        Feature[] features = new FeatureNode[sparse.getValues().length];
        int[] indices = sparse.getIndices();
        float[] values = sparse.getValues();
        for (int i = 0; i < features.length; ++i) {
            features[i] = new FeatureNode(indices[i] + 1, values[i]);
        }
        return features;
    }

    @Override
    public String apply(LabeledVector instance) {
        Feature[] feat = getFeatureArray(instance);
        return model.getLabelMap().inverse().get((int) Linear.predict(llModel, feat));
    }

    @Override
    public Map<String, Float> score(LabeledVector instance) {
        BiMap<String, Integer> labels = model.getLabelMap();
        double[] probabilities = new double[labels.size()];
        if (llModel.isProbabilityModel()) {
            Linear.predictProbability(llModel, getFeatureArray(instance), probabilities);
        } else {
            Linear.predictValues(llModel, getFeatureArray(instance), probabilities);
            float sum = 0;
            for (double probability : probabilities) {
                sum += probability;
            }
            for (int i = 0; i < probabilities.length; ++i) {
                probabilities[i] /= sum;
            }
        }

        Map<String, Float> results = new HashMap<>();
        for (Map.Entry<String, Integer> entry : labels.entrySet()) {
            results.put(entry.getKey(), (float) probabilities[entry.getValue()]);
        }
        return results;
    }

    @Override
    public void saveModel(OutputStream outputStream) {
        try {
            llModel.save(new OutputStreamWriter(outputStream));
        } catch (IOException e) {
            log.error("Error saving LibLinear model.", e);
        }
    }

    @Override
    public void loadModel(InputStream inputStream) {
        try {
            llModel = Model.load(new InputStreamReader(inputStream));
        } catch (IOException e) {
            log.error("Error loading LibLinear model.", e);
        }
    }

    public static class LiblinearModel extends BaseClassifierModel {

        @Getter
        @Setter
        private Model model;

    }

}
