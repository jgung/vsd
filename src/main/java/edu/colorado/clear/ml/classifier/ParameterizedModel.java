package edu.colorado.clear.ml.classifier;

import java.util.Map;

import edu.colorado.clear.ml.vector.LabeledVector;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public class ParameterizedModel extends BaseClassifierModel {

    @Getter
    @Setter
    private Map<String, LabeledVector> parameters;

}
