package edu.colorado.clear.ml.classifier;

import java.util.List;
import java.util.Map;

import edu.colorado.clear.ml.vector.LabeledVector;
import edu.colorado.clear.ml.api.ClassifierModel;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jamesgung
 */
public abstract class ClassifierWrapper<S extends ClassifierModel, T extends ClassifierWrapperModel<S>> extends BaseClassifier<T> {

    @Getter
    @Setter
    public BaseClassifier<S> classifier;

    @Override
    public void loadOptions(Map<String, Object> hyperparams) {
        classifier.loadOptions(hyperparams);
    }

    public abstract List<LabeledVector> loadInstances(String path, boolean training);

    public abstract BaseClassifier<S> loadClassifier();

    @Override
    public void initialize() {
        super.initialize();
        classifier = loadClassifier();
        classifier.initialize();
    }

    @Override
    public void initialize(T model) {
        super.initialize(model);
        classifier = loadClassifier();
        classifier.initialize(model.getWrappedModel());
    }

    @Override
    public void train(List<LabeledVector> train, List<LabeledVector> validation) {
        classifier.train(train, validation);
    }

    @Override
    public String apply(LabeledVector instance) {
        return classifier.apply(instance);
    }

    @Override
    public Map<String, Float> score(LabeledVector instance) {
        return classifier.score(instance);
    }

}
