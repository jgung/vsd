package edu.colorado.clear.ml.classifier;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import edu.colorado.clear.ml.api.ClassifierModel;
import lombok.Getter;

/**
 * @author jamesgung
 */
public class BaseClassifierModel implements ClassifierModel {

    @Getter
    private BiMap<String, Integer> featureMap;
    @Getter
    private BiMap<String, Integer> labelMap;

    public BaseClassifierModel() {
        featureMap = HashBiMap.create();
        featureMap.put("", 0);
        labelMap = HashBiMap.create();
    }

}
