package edu.colorado.clear.util;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class ChunkUtils {

    public static final String BEGIN = "B";
    public static final String END = "E";
    public static final String SINGLE = "S";
    public static final String IN = "I";
    public static final String OUT = "O";

    private static final String CHUNK_DELIM = "-";

    public static List<Pair<String, List<Integer>>> getChunks(List<String> labels) {
        List<Pair<String, List<Integer>>> chunks = new ArrayList<>();
        String label;
        String lastLabel = "";
        String chunkTag;
        String lastChunkTag = OUT;

        List<Integer> chunk = null;
        String currentLabel = OUT;
        int index = 0;
        for (String unit : labels) {
            label = getType(unit);
            chunkTag = getTag(unit);
            if (chunk != null) {
                if (endOfChunk(lastChunkTag, chunkTag, lastLabel, label)) {
                    chunks.add(new Pair<>(currentLabel, chunk));
                    chunk = null;
                }
            }
            if (startOfChunk(lastChunkTag, chunkTag, lastLabel, label)) {
                chunk = new ArrayList<>();
                currentLabel = label;
                chunk.add(index);
            } else if (chunk != null) {
                if (label.equals(lastLabel)) {
                    chunk.add(index);
                } else {
                    log.error("Warning: illegal tag order: {}-{} -> {}-{}", lastChunkTag, lastLabel, chunkTag, label);
                    chunks.add(new Pair<>(currentLabel, chunk));
                    chunk = null;
                }
            }
            lastLabel = label;
            lastChunkTag = chunkTag;
            ++index;
        }
        if (chunk != null) {
            chunks.add(new Pair<>(currentLabel, chunk));
        }
        return chunks;
    }

    private static boolean startOfChunk(String prevTag, String tag, String prevType, String type) {

        boolean chunkStart = false;

        if ((prevTag.equals(BEGIN) && tag.equals(BEGIN))
                || (prevTag.equals(IN) && tag.equals(BEGIN))
                || (prevTag.equals(OUT) && tag.equals(BEGIN))
                || (prevTag.equals(OUT) && tag.equals(IN))
                || (prevTag.equals(SINGLE) && (tag.equals(BEGIN) || tag.equals(SINGLE)))
                || (tag.equals(SINGLE))
                || (prevTag.equals(END) && tag.equals(END))
                || (prevTag.equals(END) && tag.equals(IN))
                || (prevTag.equals(OUT) && tag.equals(END))
                || (prevTag.equals(OUT) && tag.equals(IN))
                || (!tag.equals(OUT) && !prevType.equals(type))) {
            chunkStart = true;
        }

        return chunkStart;
    }

    private static boolean endOfChunk(String prevTag, String tag, String prevType, String type) {

        boolean chunkEnd = false;

        if ((prevTag.equals(BEGIN) && tag.equals(BEGIN))
                || (prevTag.equals(BEGIN) && tag.equals(OUT))
                || (prevTag.equals(IN) && tag.equals(BEGIN))
                || (prevTag.equals(IN) && tag.equals(OUT))
                || (prevTag.equals(SINGLE) || tag.equals(SINGLE))
                || (prevTag.equals(END) && tag.equals(END))
                || (prevTag.equals(END) && tag.equals(IN))
                || (prevTag.equals(END) && tag.equals(OUT))
                || (prevTag.equals(IN) && tag.equals(OUT))
                || (!prevTag.equals(OUT) && !prevType.equals(type))) {
            chunkEnd = true;
        }

        return chunkEnd;
    }

    private static String getType(String label) {
        String[] fields = label.split(CHUNK_DELIM);
        if (fields.length == 2) {
            return fields[1];
        } else if (fields.length == 1 && fields[0].equals(OUT)) {
            return "";
        } else {
            return label;
        }
    }

    private static String getTag(String label) {
        String[] fields = label.split(CHUNK_DELIM);
        if (fields.length == 2) {
            return fields[0];
        } else if (fields.length == 1 && fields[0].equals(OUT)) {
            return fields[0];
        } else {
            return label;
        }
    }

}
