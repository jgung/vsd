package edu.colorado.clear.util;

import com.google.common.base.Stopwatch;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class LuceneInterface {

    private IndexSearcher indexSearcher;
    private Analyzer analyzer;

    public LuceneInterface(File indexDir) {
        IndexReader iReader;
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            Directory dir = FSDirectory.open(indexDir);
            iReader = IndexReader.open(dir);
            indexSearcher = new IndexSearcher(iReader);
            analyzer = new StandardAnalyzer(Version.LUCENE_36);
            log.info("Initialized lucene index at {} ({})", indexDir.getPath(), stopwatch.stop());
        } catch (IOException e) {
            log.error("Unable to locate Lucene index.", e);
        }
    }

    public Map<String, Integer> search(String word, String field, int maxSearch) {
        Map<String, Integer> verbFreqs = new HashMap<>();
        QueryParser queryParser = new QueryParser(Version.LUCENE_36, field, analyzer);
        try {
            Query query = queryParser.parse(word);
            TopDocs topDocs = indexSearcher.search(query, maxSearch);
            ScoreDoc[] doc = topDocs.scoreDocs;
            for (int i = 0; i < maxSearch && i < doc.length; ++i) {
                int docID = doc[i].doc;
                Document d = indexSearcher.doc(docID);
                String verb = d.get("verb");
                String frequency = d.get("frequency");
                verbFreqs.put(verb, Integer.parseInt(frequency));
            }
        } catch (ParseException | IOException e) {
            log.error("Error searching Lucene index.", e);
        }
        return verbFreqs;
    }

}
