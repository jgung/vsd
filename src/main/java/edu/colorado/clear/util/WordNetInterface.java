package edu.colorado.clear.util;

import java.util.Set;

/**
 * @author jamesgung
 */
public interface WordNetInterface {

    Set<String> getHypernyms(String lemma, String pos);

}
