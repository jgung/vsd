package edu.colorado.clear.util;

import com.google.common.base.Stopwatch;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.data.ILoadPolicy;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jamesgung
 */
@Slf4j
public class JwiWordNetInterface implements WordNetInterface {

    @Getter
    private RAMDictionary dict;

    public JwiWordNetInterface(File wnDir) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            dict = new RAMDictionary(new File(wnDir, "dict"), ILoadPolicy.NO_LOAD);
            dict.open();
            dict.load(true);
            log.info("Loaded JWI WordNet RAM dict {} ({}).", wnDir.getAbsolutePath(), stopwatch.stop());
        } catch (Exception e) {
            log.error("Error loading WordNet dictionary: {}", wnDir.getName(), e);
        }
    }

    @Override
    public Set<String> getHypernyms(String string, String pennPos) {
        Set<String> words = new HashSet<>();
        POS pos = getPos(pennPos);
        if (!POS.NOUN.equals(pos)) {
            return words;
        }
        IIndexWord idxWord = dict.getIndexWord(string, pos);
        if (idxWord == null) {
            return words;
        }
        Set<ISynsetID> synsets = new HashSet<>();
        for (IWordID id : idxWord.getWordIDs()) {
            ISynset synset = dict.getWord(id).getSynset();
            synsets.add(synset.getID());
            synsets.addAll(synset.getRelatedSynsets(Pointer.HYPERNYM));
        }
        for (ISynsetID id : synsets) {
            ISynset synset = dict.getSynset(id);
            words.addAll(synset.getWords().stream().map(IWord::getLemma).collect(Collectors.toList()));
        }
        return words;
    }

    private static POS getPos(String pos) {
        if (PosUtils.isNoun(pos)) {
            return POS.NOUN;
        } else if (PosUtils.isVerb(pos)) {
            return POS.VERB;
        } else if (PosUtils.isAdjective(pos)) {
            return POS.ADJECTIVE;
        } else if (PosUtils.isAdverb(pos)) {
            return POS.ADVERB;
        } else {
            return null;
        }
    }

}
