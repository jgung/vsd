package edu.colorado.clear.util;

import lombok.Getter;

/**
 * @author jamesgung
 */
public class Pair<K, V> {

    @Getter
    private K first;
    @Getter
    private V second;

    public Pair(K first, V second) {
        this.first = first;
        this.second = second;
    }

}
