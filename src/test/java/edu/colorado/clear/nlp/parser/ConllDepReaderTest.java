package edu.colorado.clear.nlp.parser;

import org.junit.Test;

import java.io.File;
import java.util.List;

import edu.colorado.clear.nlp.type.VnSentence;

import static junit.framework.TestCase.assertEquals;

/**
 * @author jamesgung
 */
public class ConllDepReaderTest {

    private static final String dep1 = "#nw/wsj/00/wsj_0001.parse 0 8 join 22.1\tPierre Vinken , 61 years old , will join the board as a nonexecutive director Nov. 29 .\n" +
            "0\tPierre\tPierre\tNNP\tcompound\t1\t-\n" +
            "1\tVinken\tVinken\tNNP\tnsubj\t8\t-\n" +
            "2\t,\t,\t,\tpunct\t1\t-\n" +
            "3\t61\t61\tCD\tnummod\t4\t-\n" +
            "4\tyears\tyear\tNNS\tnmod:npmod\t5\t-\n" +
            "5\told\told\tJJ\tamod\t1\t-\n" +
            "6\t,\t,\t,\tpunct\t1\t-\n" +
            "7\twill\twill\tMD\taux\t8\t-\n" +
            "8\tjoin\tjoin\tVB\troot\t-1\t22.1\n" +
            "9\tthe\tthe\tDT\tdet\t10\t-\n" +
            "10\tboard\tboard\tNN\tdobj\t8\t-\n" +
            "11\tas\tas\tIN\tcase\t14\t-\n" +
            "12\ta\ta\tDT\tdet\t14\t-\n" +
            "13\tnonexecutive\tnonexecutive\tJJ\tamod\t14\t-\n" +
            "14\tdirector\tdirector\tNN\tnmod\t8\t-\n" +
            "15\tNov.\tNov.\tNNP\tnmod:tmod\t14\t-\n" +
            "16\t29\t29\tCD\tnummod\t15\t-\n" +
            "17\t.\t.\t.\tpunct\t8\t-\n";

    private static final String dep2 = "#nw/wsj/00/wsj_0002.parse 0 16 name 29.3\tRudolph Agnew , 55 years old and former chairman of Consolidated Gold Fields PLC , was named a nonexecutive director of this British industrial conglomerate .\n" +
            "0\tRudolph\tRudolph\tNNP\tcompound\t1\t-\n" +
            "1\tAgnew\tAgnew\tNNP\tnsubjpass\t16\t-\n" +
            "2\t,\t,\t,\tpunct\t1\t-\n" +
            "3\t55\t55\tCD\tnummod\t4\t-\n" +
            "4\tyears\tyear\tNNS\tnmod:npmod\t5\t-\n" +
            "5\told\told\tJJ\tamod\t1\t-\n" +
            "6\tand\tand\tCC\tcc\t5\t-\n" +
            "7\tformer\tformer\tJJ\tamod\t8\t-\n" +
            "8\tchairman\tchairman\tNN\tconj\t5\t-\n" +
            "9\tof\tof\tIN\tcase\t13\t-\n" +
            "10\tConsolidated\tConsolidated\tNNP\tcompound\t13\t-\n" +
            "11\tGold\tGold\tNNP\tcompound\t13\t-\n" +
            "12\tFields\tFields\tNNP\tcompound\t13\t-\n" +
            "13\tPLC\tPLC\tNNP\tnmod\t8\t-\n" +
            "14\t,\t,\t,\tpunct\t1\t-\n" +
            "15\twas\tbe\tVBD\tauxpass\t16\t-\n" +
            "16\tnamed\tname\tVBN\troot\t-1\t29.3\n" +
            "17\ta\ta\tDT\tdet\t19\t-\n" +
            "18\tnonexecutive\tnonexecutive\tJJ\tamod\t19\t-\n" +
            "19\tdirector\tdirector\tNN\txcomp\t16\t-\n" +
            "20\tof\tof\tIN\tcase\t24\t-\n" +
            "21\tthis\tthis\tDT\tdet\t24\t-\n" +
            "22\tBritish\tbritish\tJJ\tamod\t24\t-\n" +
            "23\tindustrial\tindustrial\tJJ\tamod\t24\t-\n" +
            "24\tconglomerate\tconglomerate\tNN\tnmod\t19\t-\n" +
            "25\t.\t.\t.\tpunct\t16\t-\n";

    @Test
    public void testReadSentences() throws Exception {
        List<VnSentence> sentenceList = new ConllDepVnReader().readSentences(new File("src/test/resources/test.dep"));
        assertEquals(dep1, sentenceList.get(0).toStringConll());
        assertEquals(dep2, sentenceList.get(1).toStringConll());
    }

    @Test
    public void testWriteConllInstances() throws Exception {
        List<VnSentence> sentenceList = new ConllDepVnReader().readSentences(new File("src/test/resources/test.dep"));
        ConllDepVnReader.writeConllInstances(sentenceList, new File("src/test/resources/testwrite.dep"));
        sentenceList = new ConllDepVnReader().readSentences(new File("src/test/resources/testwrite.dep"));
        assertEquals(dep1, sentenceList.get(0).toStringConll());
        assertEquals(dep2, sentenceList.get(1).toStringConll());
    }

}