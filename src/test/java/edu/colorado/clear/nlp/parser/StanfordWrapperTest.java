package edu.colorado.clear.nlp.parser;

import org.junit.Test;

import edu.colorado.clear.nlp.type.VnSentence;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * @author jamesgung
 */
public class StanfordWrapperTest {

    @Test
    public void testProcess() throws Exception {
        StanfordWrapper wrapper = new StanfordWrapper(true);
        VnSentence sentence = wrapper.process(wrapper.tokenize("I have a car."));
        assertTrue(sentence.getRoot().getForm().equals("have"));
    }

    @Test
    public void testTokenize() throws Exception {
        StanfordWrapper wrapper = new StanfordWrapper(false);
        assertEquals(6, wrapper.tokenize("I have $500.00 dollars.").size());
        assertEquals(5, wrapper.tokenize("You aren't going?").size());
    }


}