package edu.colorado.clear.nlp.feature;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import edu.colorado.clear.nlp.type.VnSentence;

import static junit.framework.TestCase.assertEquals;

/**
 * @author jamesgung
 */
public class ContextTest extends NlpFeatureTest {

    @Test
    public void getNgramContexts() throws Exception {
        testBigrams();
    }

    private void testBigrams() {
        VnSentence sentence = generateSentence();
        List<Context> contexts = Context.getNgramContexts(sentence.getTokens(), 2, "test", false);
        assertEquals("This", contexts.get(0).getTokens().get(0).getForm());
        assertEquals("is", contexts.get(1).getTokens().get(0).getForm());
        assertEquals("only", contexts.get(1).getTokens().get(1).getForm());
        assertEquals(".", contexts.get(5).getTokens().get(0).getForm());
        assertEquals("test{2gram}", contexts.get(0).getLabel());
        contexts = Context.getNgramContexts(sentence.getTokens(), 2, "test", true);
        assertEquals("0/test{2gram}", contexts.get(0).getLabel());
    }

    @Test
    public void getWindow() throws Exception {
        VnSentence sentence = generateSentence();
        List<Context> contexts = Context.getWindow(sentence.getTokens(), 1, Arrays.asList(-2, -1, 0, 1), "test");
        assertEquals(3, contexts.size());
        assertEquals("This", contexts.get(0).getTokens().get(0).getForm());
        assertEquals("is", contexts.get(1).getTokens().get(0).getForm());
        assertEquals("only", contexts.get(2).getTokens().get(0).getForm());
    }


}