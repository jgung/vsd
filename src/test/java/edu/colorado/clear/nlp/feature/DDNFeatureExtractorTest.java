package edu.colorado.clear.nlp.feature;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.List;

import edu.colorado.clear.ml.Feature;
import edu.colorado.clear.nlp.type.Token;
import edu.colorado.clear.util.LuceneInterface;

/**
 * @author jamesgung
 */
public class DDNFeatureExtractorTest extends NlpFeatureTest {

    @Test
    @Ignore
    public void extract() throws Exception {
        DDNFeatureExtractor extractor = new DDNFeatureExtractor(new LuceneInterface(new File("data/learningResources/ddnIndex")));
        Token token = new Token("dinner");
        token.setPos("NN");
        token.setLemma("dinner");
        List<Feature> featureList = extractor.extract(new Context("DDN", token));
        featureList.stream().map(Feature::getId).forEach(System.out::println);
    }

}