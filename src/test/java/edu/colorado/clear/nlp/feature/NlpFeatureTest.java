package edu.colorado.clear.nlp.feature;

import java.util.ArrayList;
import java.util.List;

import edu.colorado.clear.nlp.type.VnSentence;
import edu.colorado.clear.nlp.type.Token;

/**
 * @author jamesgung
 */
public class NlpFeatureTest {

    protected VnSentence generateSentence() {
        List<Token> tokens = new ArrayList<>();
        tokens.add(new Token("This"));
        tokens.add(new Token("is"));
        tokens.add(new Token("only"));
        tokens.add(new Token("a"));
        tokens.add(new Token("test"));
        tokens.add(new Token("."));
        return new VnSentence(tokens);
    }

}
