package edu.colorado.clear.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

/**
 * @author jamesgung
 */
public class ChunkUtilsTest {

    @Test
    public void testGetChunks() throws Exception {
        List<String> labels = Arrays.asList("O", "B-CAT", "B-DOG", "O", "B-CAT", "I-CAT", "O");
        List<Pair<String, List<Integer>>> chunks = ChunkUtils.getChunks(labels);
        Pair<String, List<Integer>> cat1 = chunks.get(0);
        Pair<String, List<Integer>> dog1 = chunks.get(1);
        Pair<String, List<Integer>> cat2 = chunks.get(2);
        assertEquals("CAT", cat1.getFirst());
        assertTrue(1 == cat1.getSecond().get(0));
        assertEquals("DOG", dog1.getFirst());
        assertTrue(2 == dog1.getSecond().get(0));
        assertEquals("CAT", cat2.getFirst());
        assertTrue(4 == cat2.getSecond().get(0));
        assertTrue(5 == cat2.getSecond().get(1));
    }

}