#Clear-WSD

Verb sense disambiguation system and NLP framework in Java.

## Using the CLI
A command line interface is provided as an executable Jar (downloadable [here](https://www.dropbox.com/s/tf9yngz59sq27s6/WsdCli.jar?dl=0)) with the following options:

~~~shell
  Options:
    --apply
       apply classifier to test data and display results
       Default: false
    --cv
       perform sampled, stratified cross validation on the training data
       Default: false
    --errOnly
       only display erroneous classifications
       Default: true
    --itl
       engage interactive loop for testing
       Default: false
    --skipParse
       skip the parsing step on the data
       Default: false
    --test
       test on the testing data
       Default: false
    --train
       train a new model on the given data
       Default: false
    -data
       path to the classifier data directory
       Default: edu/colorado/clear/nlp/models/feature-extractors.model
    -folds
       number of cross validation folds to sample
       Default: 10
  * -model
       path to the classifier model
    -per
       percentage of instances to use for training in each fold
       Default: 0.8
    -test
       path to testing instances
    -train
       path to training instances
    -valid
       path to validation instances
~~~

You can also use the provided CLI to perform cross validation, test utterances interactively, or apply a pre-trained model to new data.

### Training

The trainer expects files with instances as lines in the following format:
~~~text
path <space> sentence# <space> token# <space> lemma <space> class_label <tab> sentence_text
~~~
For example:
~~~text
example.txt 25 3 get comprehend-87.2-1	Oh , I get it .
example.txt 57 2 get get-13.5.1-1	Did you get that part ?
~~~

To train a new model, run the ClassifierCli with the following arguments:
~~~shell
java -jar WsdCli.jar -train path/to/training-file.txt -model path/to/model.bin --train
~~~
If you have a development set of data, you can use it for early stopping during training like so:
~~~shell
java -jar WsdCli.jar -train path/to/training-data.txt -valid path/to/devel-data.txt -model path/to/model.bin --train
~~~
You should see output like the following during parsing:
~~~shell
10:37:58.472 [main] INFO  - Reading instances...
10:37:58.474 [main] INFO  - 0.00%
10:37:59.474 [main] INFO  - 16.41%
...
~~~
After parsing, you will see something like the following during training. Training will continue until performance on the validation data (or training data if now validation data is provided) does not improve for 10 epochs.
~~~shell
10:38:07.574 [main] INFO  - Loaded 1523 pre-parsed instances from path/to/training-data.txt.dep in 43.06 ms
10:38:07.720 [main] INFO  - Loaded features for 1523 instances from path/to/training-data.txt.dep in 143.1 ms
10:38:07.721 [main] INFO  - Beginning training (1523 train instances, 0 validation instances)
10:38:07.730 [main] INFO  - Epoch 0: 437 / 1523 incorrect.
10:38:07.731 [main] INFO  - 10 epochs remaining.
10:38:07.734 [main] INFO  - Epoch 1: 233 / 1523 incorrect.
10:38:07.734 [main] INFO  - 10 epochs remaining.
10:38:07.737 [main] INFO  - Epoch 2: 173 / 1523 incorrect.
10:38:07.737 [main] INFO  - 10 epochs remaining.
...
~~~
After training completes, a model will be saved to the model path supplied (overwriting any pre-existing model).

## Running from sources

To compile and run directly from the source code, you must use Maven.

~~~shell
git clone https://jgung@bitbucket.org/jgung/vsd.git
cd vsd
mvn clean install
# you can use the maven-exec-plugin to run the cli:
mvn exec:java -Dexec.mainClass="edu.colorado.clear.app.WsdCli" -Dexec.args="-train path/to/training-data.txt -model model.vn --train --itl"
# to build the CLI as a runnable jar saved to target/WsdCli.jar:
mvn clean compile assembly:single
~~~